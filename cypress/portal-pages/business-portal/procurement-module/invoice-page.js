import {getNextDay} from "../../../support/data"

class InvoicePage {
    let
    invoice1;
    let
    invoiceNumber;
    invoiceNumber = this.invoice1

    navigateToInvoiceScreen() {
        cy.waitUntil(()  =>this.getSolutionsMenu()).click()
        this.getInvoice().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }

    getTableElement() {
        return cy.waitUntil(() => cy.get("table"))
    }

    validateTableHavingData() {
        this.getTableElement().getTable().should(tableData => {
            cy.waitUntil(() => expect(tableData)).not.to.be.empty
        })
    }

    getInvoice() {
        return cy.contains(" Invoice ")
    }

    getSolutionsMenu() {
        return cy.get("#navbarDropdownMenuLink-solutions")
    }

    getNewInvoice() {
        return cy.contains("New Invoice")
    }

    clickNewInvoice() {

        this.getNewInvoice().click()
    }

    getInvoiceType() {
        return cy.get(" p-dropdown[placeholder='Select Invoice Type']")
    }

    getCurrency() {
        return cy.get(" p-dropdown[placeholder='Select Currency']")
    }

    getSelectPOnumber() {
        return cy.get("p-autocomplete[placeholder='Select PO Number']")
    }

    getSelectPaymentTerm() {
        return cy.get("p-dropdown[id='paymentTerms']").children().children(":nth-child(4)")
    }

    getInvoiceLineItems() {
        return cy.contains("Invoice Details")
    }

    getGeneralLedger() {
        return cy.get("p-dropdown[id='glName']")
    }

    selectGeneralLedger(generalLedger) {
        this.getGeneralLedger().children().children(":nth-child(4)").click()
        cy.contains(generalLedger).dblclick()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(4000)
    }

    getProductName() {
        return cy.get("#productName")

    }

    selectProduct(product) {
        /* this.getProductName().children().children(":nth-child(4)").dblclick()
        cy.contains(product).click()*/

        cy.get("[data-test-class=\"drpdwn productName\"]").children().children(":nth-child(4)").children().click()
        cy.contains(product).click()
        /*  cy.get("#productName > .ui-dropdown > .ui-dropdown-label-container > .ui-dropdown-label").click()

        cy.get(".ui-dropdown-item > .ng-star-inserted").click()*/
    }

    getInvoiceLineItemsAddButton() {
        cy.contains("Select Product").parent().parent().parent().parent().children(":nth-child(3)").children(":nth-child(1)").click()
    }

    getClearButton() {
        return cy.contains("Clear")
    }

    clickClearButton() {
        this.getClearButton().click()
    }

    getInvoiceQuantity() {
        return cy.get("#quantityInvoiced")
    }

    getPrice() {
        return cy.get("#unitSellingPrice")
    }

    getTotalTax() {
        return cy.get("#totalTax")
    }

    enterTax(tax) {
        this.getTotalTax().clear()
        this.getTotalTax().type(tax)
    }

    adddate() {
        cy.get(".ui-calendar > .ui-inputtext").click()
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el)
                    .click()
            }
        })
    }

    validateSelectedLineItem() {
        cy.get("table tbody tr td").each(($e1, index, $list) => {
            var productName = $e1.text()
            cy.get("table tbody tr td").should("have.text", productName)
            // if(product == productName)

        })
    }

    clickAddNonProductLine() {
        return cy.contains("Add Non-product Line")
    }

    enterReason(reason) {
        cy.get("#reason").type(reason)
    }

    enterInvoiceAmount(amount) {
        cy.get("#invoiceAmount").type(amount)
    }

    enterInvoiceLineItems(lineItems) {
        this.clickInvoiceDetails()
        this.selectGeneralLedger(lineItems.generalLedger)
        this.getTotalTax().click()
        this.selectProduct(lineItems.product)
        ///this.selectProduct()
        this.enterTax(lineItems.tax)
        // this.getInvoiceLineItemsAddButton()
        cy.contains("Add").click({force: true})
    }

    enterInvoiceLineItemsApprovedPO(lineItems) {
        this.clickInvoiceDetails()
        this.selectGeneralLedger(lineItems.generalLedger)
        this.getTotalTax().click()
        this.selectProduct(lineItems.approvedPoProduct)
        ///this.selectProduct()
        this.enterTax(lineItems.tax)
        cy.get("[data-test-class=\"ip quantityInvoiced\"]").clear().type(lineItems.quantity)
        // this.getInvoiceLineItemsAddButton()
        cy.contains("Add").click({force: true})
    }


    enterNonProductLineItems(nonProductLineItems) {
        this.clickInvoiceDetails()
        this.clickAddNonProductLine().click({force: true})
        this.selectItem(nonProductLineItems.item)
        this.getReason().type(nonProductLineItems.reason)
        this.getInvoiceAmount().type(nonProductLineItems.amount)
        cy.get(".form-horizontal > .text-right > .ng-star-inserted > .ui-button-text").click({force: true})
    }

    getItemDropdown() {
        return cy.get("p-dropdown[id='productId']").children().children(":nth-child(4)")
    }

    selectItem(item) {
        this.getItemDropdown().click({force: true})
        cy.contains(item).click({force: true})
    }

    getReason() {
        return cy.get("#reason")
    }

    getInvoiceAmount() {
        return cy.get("#invoiceAmount")
    }

    getInvoiceApprovers() {
        return cy.get("#ui-tabpanel-2-label")
    }

    getAddApproversButton() {
        return cy.contains("Add Approvers")
    }

    clickApproversTab() {
        this.getInvoiceApprovers().click()
    }

    addApprover(approver) {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.contains(approver).parent().children(":nth-child(1)").children().children().click()
        cy.get("app-invoice-approvers-details button[label='Add']").click({force: true})
    }

    clickSupportingDocuments() {
        cy.contains("Supporting Documents").click()
    }

    getApproverAddButton() {

        return cy.contains("Add")
    }

    getChooseOption() {
        cy.get("input[type='file']")
    }

    uploadSupportingDocument(filePath) {
        const path = filePath
        this.getSupportingDocuments().click()
        cy.get("input[type='file']").attachFile(path)
        cy.contains("Upload").click()

    }

    getSupportingDocuments() {
        return cy.contains("Supporting Documents")
    }

    getSubmitbutton() {
        return cy.contains("Submit")
    }

    clickSubmitButton() {
        this.getSubmitbutton.click()
    }

    getDropdownOption() {
        return cy.get("p-autocomplete[placeholder='Select PO Number'] li div")
    }

    selectInvoiceType(type) {
        this.getInvoiceType().children().children(":nth-child(4)").click({force: true})
        cy.waitUntil(() => cy.contains(type)).click({force: true})

    }

    SelectCurrency(Currency) {
        this.getCurrency().children().children(":nth-child(4)").click({force: true})
        cy.contains(Currency).click({force: true})
    }

    selectPaymentTerm(paymentTerm) {
        this.getSelectPaymentTerm().click({force: true})
        cy.contains(paymentTerm).click({force: true})

    }

    selectPoNumber(po) {
        this.getSelectPOnumber().type("PO00")
        this.getDropdownOption().each(($e1, index, $list) => {
            var PoNumber = $e1.text()
            if (PoNumber == po) {
                cy.wrap($e1).click({force: true})
            }
        })
    }

    selectApprovedPoNumber(approvedPo) {
        this.getSelectPOnumber().type("PO00")
        this.getDropdownOption().each(($e1, index, $list) => {
            var PoNumber = $e1.text()
            if (PoNumber == approvedPo) {
                cy.wrap($e1).click({force: true})
            }
        })
    }

    getMyTaskOption() {
        cy.contains("My Tasks").click()
    }

    getSettingsIcon() {
        return cy.get("#navbarDropdownMenuLink-settings")
    }

    goToConfiguration() {
        this.getSettingsIcon().click()
        this.getMyTaskOption()
    }

    getAddButton() {
        return cy.contains("Add")
    }

    clickAddButton() {
        this.getAddButton().click({force: true})
    }

    getSaveButton() {
        return cy.contains("Submit")
    }

    clickSaveButton() {
        this.getSaveButton().click()
    }

    getSuccessPopup() {
        return cy.get(".ng-star-inserted > div > b")
    }

    validateCreatedInvoice(successMessage) {
        this.getSuccessPopup().invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            // var pattern = ([A-Za-z0-9]+)$
            let invoiceId = fullText.match(pattern)
            cy.log(invoiceId)
            this.getSuccessPopup().should("have.text", successMessage + invoiceId)
            cy.log()
            cy.log("Invoice created successfully!")
            //this.getSuccessPopup().should("have.text", "Invoice Created successfully")
            this.invoice1 = "INV" + invoiceId
            cy.log(this.invoice1)
        })
    }

    checkInvoiceId() {
        cy.get("a[class=\"ui-paginator-last ui-paginator-element ui-state-default ui-corner-all\"]").eq(0).click({ force : true })
        // cy.get("#ui-tabpanel-6 > .col-md-8 > .ui-table > p-paginator.ng-star-inserted > .ui-paginator-bottom > .ui-paginator-last > .ui-paginator-icon").click()
        cy.wait(2000)
        cy.contains(this.invoice1).click()

    }


    checkInvoice() {
        cy.get("tbody tr").each(($e1, index, $list) => {
            if ($e1.text() == this.invoice1) {
                cy.contains(this.invoice1).click()
            } else {
                if ($e1.text() != this.invoice1) {

                    cy.waitUntil(() => cy.get("#ui-tabpanel-3 > .col-md-8 > .ui-table > p-paginator.ng-star-inserted > .ui-paginator-bottom > .ui-paginator-pages > :nth-child(2)")).click()
                    cy.contains(this.invoice1).click()
                }
            }
        })
    }


    approveInvoice() {
        cy.get("textarea").type("approved")
        cy.contains("Approve").click()
    }

    rejectInvoice() {
        cy.get("textarea").type("rejected")
        cy.contains("Rejected").click()
    }

    validateApprovedInvoiceStatus() {

        cy.waitUntil(() => cy.url().should("include", "invoices"))
        cy.get("tbody tr").each(($el, index, $list) => {
            cy.log($el)
            let invoice = $el.text()
            cy.log(invoice)
            if (invoice == this.invoice1) {
                cy.contains(this.invoice1).children(cy.getBySel("td status")).should("have.text", "APPROVED")
            }
        })
    }

    validateRejectedInvoiceStatus() {
        cy.waitUntil(() => cy.url().should("include", "invoices"))
        cy.get("tbody tr").each(($el, index, $list) => {
            cy.log($el)
            let invoice = $el.text()
            cy.log(invoice)
            if (invoice == this.invoice1) {
                cy.contains(this.invoice1).children(cy.getBySel("td status")).should("have.text", "REJECTED")
            }
        })
    }

    clickOkButton() {
        cy.contains("OK").click()
    }

    clickInvoiceDetails() {
        this.getInvoiceLineItems().click({force: true})
    }

    invoiceDate(effectiveDate) {
        var todaysDate = moment().format("DD")
        var formatedDate = todaysDate.replace(/^0+/, "")
        cy.get("#effectiveFrom > .ui-calendar > .ui-inputtext").click()
        cy.get("#effectiveFrom  td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == formatedDate) {
                cy.wrap($el).click()
            }
        })
    }

    enterGeneralDetails(general) {
        this.selectInvoiceType(general.type)
        this.SelectCurrency(general.currency)
        this.adddate()
    }

    selectReceivedPO(po) {
        this.selectPoNumber(po)
    }

    providePaymentTerm(paymentTerm) {
        this.selectPaymentTerm(paymentTerm)
    }

    enterGeneralSectionDetails(general) {
        this.selectInvoiceType(general.type)
        this.SelectCurrency(general.currency)
        this.adddate()
        // this.selectPoNumber(general.approvedPo)

    }

    enterInvoiceDetials() {
        this.getInvoiceLineItems()
    }

    getInvoiceApprovers() {
        return cy.contains("Invoice Approvers")
    }

    getAddApproverButton() {
        return cy.contains("Add Approvers")
    }

    clickInvoiceApprovers() {
        this.getInvoiceApprovers().click({force: true})
        this.getAddApproverButton().click({force: true})

    }


    validateAlreadyCreatedInvocie() {
        cy.get("app-custom-info-dialog div b").should("have.text", validationMessage)
        cy.contains("Ok").click()
    }

    clickOkButton() {
        return cy.get(".left-right-margin-2px > .ui-button-icon-left").click()
    }

    getPoValidation() {
        //return cy.get("app-custom-info-dialog").children().children(":nth-child(1)")
        return cy.get(".ng-star-inserted > div > b")
    }
    validateApprovedPO(){
        cy.get(".ng-star-inserted > div > b").should("have.text","Invoice quantity cannot exceed quantity received.")
        cy.contains("Ok").click()
    }


    validatePOToCreateInvoice(poValidation) {

        cy.waitUntil(()  => this.getPoValidation().invoke("text")).then((text) => {
            this.getPoValidation().should("have.have", poValidation)
            cy.log("Selected Purchase order is not received")

        })

    }
}

export default InvoicePage