/// <reference types="cypress" />

class CatalogueBuyingPage {
    let
    por1;

    navigateToMarketPalceScreen() {
        cy.waitUntil(() => this.getSolutionsMenu()).click()
        this.getMarketPlace().click()
    }



    goToMarketPlace() {
        cy.waitUntil(() => this.getMarketPlace()).click({force: true})
    }

    getSolutionsMenu() {
        return cy.get("#navbarDropdownMenuLink-solutions")
    }
    clickMarketplace(){
        cy.waitUntil(() =>cy.contains("Marketplace")).click()
    }

    getMarketPlace(){
        return cy.contains(" Marketplace ")
    }

    getCatalogueLink(catalogue){
        cy.contains(catalogue).click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(6000)
    }

    increaseQuantity(){
        return cy.get(".left-right-margin-2px > .ui-button-text")

    }

    clickIncreaseQuantity(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
       // cy.wait(2000)
        cy.waitUntil(() => this.increaseQuantity()).click()
    }

    clickAddToCartButton(product){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.contains(product).each(($el,index,$list) => {
            if ($el.text().includes(product)) {
                // eslint-disable-next-line cypress/no-unnecessary-waiting
                cy.wait(3000)
                cy.contains(product).parent().parent().parent().parent().children(":nth-child(3)").children(":nth-child(2)").click()
            }
        })
    }

    getMyCart(){
        return cy.get("#togglercart-button")
    }

    getProceed(){
        return cy.contains("Proceed")
    }
    clickMyCart(){
        this.getMyCart().click()
        this.getProceed().click()
    }

    getDepartment(){
        return cy.get("#department")
    }

    selectDepartment(department){
        //cy.get("#department > .ui-dropdown > .ui-dropdown-label-container > .ui-dropdown-label").click()
        cy.contains(department).click({force: true})
        // this.getCostcenter().click()

    }

    getCostcenter(costCenter){
        cy.get("#costCenter").click({force: true})
        cy.contains(costCenter).click({force: true})
    }

    getDeliveryAddress(){
        return cy.contains("Delivery Address")
    }


    getSupplierDropdown(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.get("#suppliers").click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.contains("Tyrion").click()
        // cy.wait("4000")

    }

    getAddress(){
        return cy.contains("Select Delivery Address")
    }

    selectDeliveryAddress(deliveryAddress){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        // cy.get(":nth-child(1) > .ui-dropdown-item > .ng-star-inserted").click()
        cy.contains(deliveryAddress).click({force: true})

    }

    getContractSection(){
        return cy.contains("Contract Selection")
    }
    clickContractSection(){
        this.getContractSection().click()
    }
    getCheckOutButton(){

        return cy.get(".center > .ui-button-text")
    }

    getYesButton(){

        return cy.contains("Yes")
    }

    clickOKButton(){
        cy.get(".left-right-margin-2px > .ui-button-text").click()

    }
    clickYesButton(){
        this.getYesButton().click()
    }
    clickCheckoutbutton(){
        this.getCheckOutButton().should("not.be.disabled")
        this.getCheckOutButton().click()
    }

    validateCheckoutbutton(){
        cy.get(".center > .ui-button-text").should("be.visible")
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(5000)
        cy.get("div[class='subtotal cf'] ul li button").should("be.disabled")


        //this.getCheckOutButton().click()
    }

    enterDeliveryAddress(address){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.contains("Delivery Address").scrollIntoView()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)

        this.getDeliveryAddress().click()
        this.getDepartment().click({force: true})
        this.selectDepartment(address.department)
        this.getCostcenter(address.costCenter)
        this.getAddress().click({force: true})
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        this.selectDeliveryAddress(address.deliveryAddress)
    }
    getSuccessPOR(){
        return cy.get(".ng-star-inserted > div > b")
    }

    validatePORAfterCreation(){
        cy.url().should("include", "po-requisition-list")
    }

    selectContract(contract){
        cy.contains("Select Contract").click({force: true})
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.contains(contract).click()

    }

    getSelectQuantity() {
        return cy.get(".ui-spinner-input")
    }
    getCancelButton(){
        return cy.contains("Cancel")
    }

    clickCancelButton(){
        this.getCancelButton().click()
    }

    enterQuantity(){
        this.getSelectQuantity().clear()
        this.getSelectQuantity().type("0")
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }

    addMultipleItems(items){
        this.getSelectQuantity().clear()
        this.getSelectQuantity().type(items.count)
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)

    }
    ValidateAddTOCartButton(){
        cy.contains("Add To Cart").should("be.disabled")

    }
    getSuccessPopup(){
        return cy.get("body > p-dynamicdialog  app-custom-info-dialog > div > b")
    }

    validatePOR(successMessage) {
        this.getSuccessPopup().invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            var PORNumber = fullText.match(pattern)
            cy.log(PORNumber)
            this.getSuccessPOR().should("have.text", successMessage + PORNumber)
            cy.log("POR created successfully")
            this.por1 = PORNumber
        })
    }

    getPOR(){
        return cy.contains(" PO Requisition Listing ")
    }

    navigateToPOR(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        this.getSolutionsMenu().click()
        this.getPOR().click()
    }

    getTableElement() {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        return cy.get("table")
    }

    validateTableHavingData() {
        this.getTableElement().getTable().should(tableData => {
            // cy.wait(4000)
            expect(tableData).not.to.be.empty
        })
    }


    validateCreatedPOR(por){
        cy.get("tbody tr td").each(($el, index, $list) => {
            var requisition = $el.text()
            if (requisition == por) {
                cy.log("POR is vaildated")
            }
        })
    }


    checkPOR() {
        cy.get("tbody tr").each(($e1, index, $list) => {
            if ($e1.text() == this.por1) {
                cy.contains(this.por1).click()
            }
        })
    }

    goToMarketplace(){
        this.getSolutionsMenu().click()
        this.goToMarketPlace().click()
    }
    /*validateCreatedPOR(por) {
        cy.get(".ui-table-tbody tr:nth-child(1)  a:nth-child(1)").then((por) => {
            expect(por).to.contain.text(por)
        })
    }*/

}

export default CatalogueBuyingPage
    