/// <reference types="cypress" />
import {getNextDay, getTodayDate} from "../../../support/data"

class ProductsPage {
    householdDropdown = "#navbarDropdownMenuLink-household";
    procurementMenu = "[data-test-class=\"href procurement\"]";

    navigateToProductScreen() {
        cy.get(this.householdDropdown).click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.get(this.procurementMenu).click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        if (cy.get("preloader").should("not.exist")) {
            cy.contains(" Product ").click({force: true})
        }
    }

    getTableElement() {
        return cy.waitUntil(() => cy.get("table"))
    }

    validatePagination() {
        cy.get("tr > :nth-child(1)").should("be.length", 11)
    }

    validateTableColumns() {
        cy.get("[data-test-class=\"th product-code\"]").should("contain.text", "Product Code")
        cy.get("[data-test-class=\"th external-id\"]").should("contain.text", "External ID")
        cy.get("[data-test-class=\"th name\"]").should("contain.text", "Name")
    }

    validateTheProductListingScreen() {
        cy.url().should("include", "/home/procurement/product")

    }

    validateTableHavingData() {
        cy.waitUntil(() => this.getTableElement()).getTable().should(tableData => {
            expect(tableData).not.to.be.empty
        })
    }


    getNewProductButton() {
        return cy.get("[data-test-class=\"btn createProduct\"]")
    }

    getProductCodeField() {

        return cy.get("[data-test-class=\"ip productCode\"]")
    }

    getExternalIdField() {
        return cy.get("[data-test-class=\"ip externalId\"]")
    }

    getNameField() {
        return cy.get("[data-test-class=\"ip name\"]")
    }

    getDescriptionField() {
        return cy.get("#description")
    }

    selectProductGroup(productGroup) {
        cy.contains(productGroup).click()
        cy.get("[data-test-class=\"pklst productGroup\"] div div [icon='pi pi-angle-right']").click()
    }

    selectSupplier(supplier) {
        cy.contains(supplier).click()
        cy.get("[data-test-class=\"pklst Supplier\"]")
    }

    selectGeneralLedger(ledger) {
        cy.get("[data-test-class=\"drpdwn GeneralLedger\"] div div [icon='pi pi-angle-right']").click()
        this.getDropdownOption().contains(ledger).click()
    }

    getDropdownOption() {
        return cy.get("p-dropdownitem > li")
    }

    selectCpvCode(code) {
        cy.get("[data-test-class=\"drpdwn CpvCodeDTO\"]").click()
        this.getDropdownOption().contains(code).click()
    }

    selectCostAccount(costaccount) {
        cy.get("#CostAccountDTO  label").click()
        this.getDropdownOption().contains(costaccount).click()
    }

    getCostPrice() {
        return cy.get("#costPrice")
    }

    getSalesPrice() {
        return cy.get("#salesPrice")
    }

    getMarginPrice() {
        return cy.get("#margin")
    }

    getTaxPrice() {
        return cy.get("#taxPercentage")
    }

    addPriceDetails(costprice, salesprice, margin, tax) {
        this.gotoPriceTab()
        this.getCostPrice().type(costprice)
        this.getSalesPrice().type(salesprice)
        this.getMarginPrice().type(margin)
        this.getTaxPrice().type(tax)
    }


    addProductCoverImage(coverImage) {
        const yourFiturePath = "business-portal/procurement-module/images/coverimage.jpg"
        cy.contains("Images").click()
        cy.get("p-fileupload.ng-star-inserted > .ui-button > .ng-star-inserted").attachFile(yourFiturePath)

    }

    getProductImages() {
        return cy.get(".ui-fileupload-choose > input")
    }

    addProductImages(images) {

        //   cy.get(".ui-fileupload-choose > input").attachFile(images.image1)
        cy.get(".ui-fileupload-buttonbar > .ui-fileupload-choose > input").attachFile(images.image1)
            .attachFile(images.image2)
            .attachFile(images.image3)
            .attachFile(images.image4)
            .attachFile(images.image5)
        this.getUploadButton().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(4000)
    }

    getUploadButton() {
        return cy.get("[icon=\"pi pi-upload\"] > .ui-button > .ui-button-text")
    }

    getSaveButton() {
        return cy.get("[data-test-class=\"btn submit\"]")
    }

    submitProductDetails() {
        this.getSaveButton().click({force: true})
    }

    validateProductCreated(productName) {
        cy.get("tbody td").contains(productName).then((product) => {
            expect(product).to.contain.text(productName)
        })

        cy.log("Product created successfully")
    }

    getDialogue() {
        return cy.get(".p-confirm-dialog-message")
    }

    validateCreatedProduct(){
        this.getDialogue().should("be.visible", "Product Created successfully!")
    }


    openNewProductPage() {
        cy.get("[data-test-class=\"btn createProduct\"]").click()
    }

    addProductName(productName) {
        this.getNameField().type(productName)
    }

    addProductCode(productCode) {
        this.getProductCodeField().clear().type(productCode)
    }

    addExternalId(id) {
        this.getExternalIdField().type(id)
    }

    addDescription(description) {
        this.getDescriptionField().type(description)
    }

    getEditIcon(productName) {
        return cy.contains(productName).parent("[data-test-class=\"td name\"]").siblings("[data-test-class=\"td actions\"]").children("[data-test-class=\"href editProduct\"]")
    }

    openEditProductView(productName) {
        this.getEditIcon(productName).click()
    }

    getDeleteButton() {
        return cy.contains("Delete")
    }

    deleteProduct(productName) {
        this.selectProduct(productName)
        this.getDeleteButton().click()
    }

    selectProduct(productName) {

        this.getSelectionCheckBox(productName).click()

    }

    navigateToviewproduct(productName) {
        // cy.contains(productName).click()
        cy.get("[data-test-class=\"td name\"]").eq(0).click()
    }

    validateViewProductScreen() {
        cy.url().should("include", "product/view/")

    }

    getSelectionCheckBox(productName) {
        return cy.contains(productName).parent("[data-test-class=\"td name\"]").siblings("[data-test-class=\"td sl-no\"]").children("[data-test-class=\"tbl-chkbx select-product\"]")
    }

    validateDeletedProductDetailsInDB(productName) {
        const query = "select * from ep_product_service.product p where name =\"" + productName + "\";"
        cy.log(query)
        cy.task("queryDb", query)
            .then(function (result) {
                if (result.length == 0) {
                    cy.log("Product is not available in DB , Delete action is successful")
                } else {
                    cy.log("retrieved the object of the query")
                    cy.log("Product name is ", result[0].name)
                    if (result[0].name == productName) {
                        cy.log("")
                        throw new Error("Product is not deleted from DB")
                    }
                }
            })
    }

    getDialogue() {
        return cy.get(".p-confirm-dialog-message")
    }

    validateDuplicateProductDetails() {
        this.getDialogue().should("to.be.visible")
    }

    getMoreOption() {
        return cy.get("#navbarDropdownMenuLink-more")
    }

    getCloneOption() {
        return cy.contains(" Clone Product ")
    }

    navigateToCloneProduct() {
        this.getMoreOption().click()
        this.getCloneOption().click()
    }

    updateDescription(updatedDescription) {
        this.getDescriptionField().clear()
        this.getDescriptionField().type(updatedDescription)
        this.submitProductDetails()
    }

    addEffectiveDate() {
        cy.get("#effectiveFrom > .ui-calendar > .ui-datepicker-trigger > .ui-button-icon-left").click({force: true})
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getTodayDate()) {
                cy.wrap($el).dblclick()

            }
        })
    }

    addEffectiveTillDate() {
        this.gotoPriceTab().click()
        cy.waitUntil(() =>  cy.get("#effectiveTill")).click()
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el).dblclick()
            }
        })
    }

    validateUpdatedDescDetails(ProductName, updatedDescription) {
        cy.get(".p-confirm-dialog-message").should("have.text", "Product Updated successfully!")
        //  cy.contains(ProductName).parent("[data-test-class=\"td name\"]").siblings("[data-test-class=\"td description\"]").should("have.text", updatedDescription)
    }

    validateDeletedproduct(ProductName) {
        cy.get(".p-confirm-dialog-message").should("have.text", "Selected record(s) deleted successfully!")
        // cy.contains(ProductName).should("not.be.visible")
    }

    validateSaveButtonDisabled() {
        this.getSaveButton().should("be.disabled")
    }

    validateProductCodeRequiredMessage(productCodeRequiredMessage) {
        this.getProductCodeField().clear()
        this.getDescriptionField().click()
        this.getProductCodeField().parent().children(":nth-child(2)").should("have.text", productCodeRequiredMessage)
    }

    validateProductNameErrorMessage(productNameValidationMessage) {
        this.getDescriptionField().click()
        this.getNameField().parent().children(":nth-child(2)").should("have.text", productNameValidationMessage)
    }


    clearProductField() {
        this.getNameField().click()
        this.getNameField().clear()
    }

    validatePriceErrorMessage(PriceRequiredMessage) {
        this.getPriceValidation().should("have.text", PriceRequiredMessage)
    }

    deletePrice() {
        this.getDeleteIcon().click()
    }

    getPriceValidation() {
        return cy.get("app-price p")
    }

    getDeleteIcon() {
        return cy.get("[ptooltip=\"Delete\"]")
    }

    gotoPriceTab() {
        return cy.contains("Price").click()
    }

    createProduct(productDetails) {
        this.openNewProductPage()
        this.addProductCode(productDetails.ProductCode)
        this.addExternalId(productDetails.ExternalId)
        this.addProductName(productDetails.ProductName)
        this.addDescription(productDetails.Description)
        this.selectProductGroup(productDetails.ProductGroup)
        this.selectCpvCode(productDetails.CPVCode)
        this.selectCostAccount(productDetails.costAccount)
        this.gotoPriceTab().click()
        this.addEffectiveDate()
        this.addEffectiveTillDate()
        this.addPriceDetails(productDetails.CostPrice, productDetails.SalesPrice, productDetails.Margin, productDetails.Tax)
        this.addProductCoverImage()
        this.addProductImages(productDetails)
    }

    enterProductCloneDetails(){
        this.addProductCode(productDetails.ProductCode)
        this.addExternalId(productDetails.ExternalId)
        this.addProductName(productDetails.ProductName)
        this.addDescription(productDetails.Description)

    }

    clickSubmitButton(){
        this.submitProductDetails()
    }
}

export default ProductsPage
