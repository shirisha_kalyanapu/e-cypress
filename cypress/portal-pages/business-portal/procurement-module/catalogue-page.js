/// <reference types="cypress" />

class CataloguePage {

    addCatalogueName(catalogue) {
        this.getCatalogueName().type(catalogue)
    }

    clickCatalogue() {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.contains(" Catalogue ").click({force: true})
    }

    getNewCatalogue() {

        return cy.get("[data-test-class=\"btn createCatalogue\"]").click({force: true})
    }

    getCatalogueDescription() {
        return cy.get("[data-test-class=\"ip description\"]")
    }

    addCatalogueDescription(desc) {
        this.getCatalogueDescription()
            .clear()
            .type(desc)
    }

    validateUpdatedDescDetails(catalogName, updatedDescription) {
        this.getDialogue().should("have.text", "Catalogue Updated successfully!")
        // cy.contains(catalogName).parent().parent().children(":nth-child(3)").should("have.text", updatedDescription)
    }

    getCatalogueName() {
        return cy.get("[data-test-class=\"ip name\"]")
    }

    getNameErrorMessage() {
        return cy.get(".invalid-feedback > .ng-star-inserted")
    }

    validateCatalogueName() {
        this.getCatalogueDescription().click()
        cy.get(".invalid-feedback > .ng-star-inserted").should("have.text", "name can not be longer than 25 characters")
    }

    validateDescription() {
        this.getCatalogueName().click()
        cy.get(".invalid-feedback > .ng-star-inserted").should("have.text", "Description can not be longer than 256 characters")
    }

    getCatalogueType() {
        cy.get(".ui-dropdown-label-container > .ng-tns-c13-6").click()
        return cy.get("p-dropdownitem > li > span")
    }

    addCatalogueType(type) {

        cy.waitUntil(() => cy.get("[data-test-class=\"drpdwn catalogueType\"]")).click()
        cy.get("p-dropdownitem > li > span").each(($el, index, $list) => {
            var catalogueType = $el.text()
            if (catalogueType == type) {
                cy.wrap($el).click({force: true})
            }

        })
    }
    clickSaveButton(){
        cy.get("[label=\"Submit\"]")
            .click()
    }

    CatalogueType(type) {
        cy.waitUntil(() => cy.get("[data-test-class=\"drpdwn catalogueType\"]")).click()
        cy.get("p-dropdownitem > li > span").each(($el, index, $list) => {
            var catalogueType = $el.text()
            if (catalogueType == type) {
                cy.wrap($el).click({force: true})
            }
        })
    }

    attachCoverImage(img) {
        this.getCoverImageChooseButton().attachFile(img)
    }

    getCoverImageChooseButton() {
        return cy.get(".ui-button > .ng-star-inserted")
    }

    createCatalogue() {
        cy.fixture("catalogueDetails").then(function (data) {
            this.data = data
            cy.contains("New Catalogue").click()
            cy.get("#name").type(this.data.CatalogueName)
            cy.get("#description").type(this.data.Description)
            this.addCatalogueType(this.data.catalogueType)
            cy.get(".ui-button > .ng-star-inserted").attachFile(this.data.image1)
            cy.get("[label=\"Submit\"] > .ui-button-text").click()
            cy.get("[label=\"Save\"] > .ui-button-text").click()
            this.getDialogue().should("have.text", catalogueNameValidation)


        })
    }

    clickOnAddProduct() {
        cy.get("[data-test-class=\"btn addProduct\"]").click()
    }

    addProduct(product) {
        this.clickOnAddProduct()
        //cy.get("[data-test-class=\"btn searchProduct\"]")
        //cy.wait(3000)
        cy.get(".col-3 > .mr-1 > .ui-button-text").click()
        cy.get("table td").each(($el, index, item) => {
            var productName = $el.text()
            if (productName == product) {
                cy.get("[data-test-class=\"td actions\"]").eq(index - 1).click().then(() => {
                    //cy.get("[data-test-class=\"td actions\"]").eq(index - 1).should("be.disabled")
                })
            }
        })
    }

    clickSubmitButton() {
        cy.get(".float-right > [data-test-class=\"btn submit\"] > .ui-button-text").click()
        //cy.get("[data-test-class=\"btn submit\"]").click()
    }

    validateSubmitButton() {
        //cy.get("[data-test-class=\"btn submit\"]").should("be.visible")
        cy.get(".float-right > [data-test-class=\"btn submit\"] > .ui-button-text").should("be.visible")
    }

    validateSaveButton() {
        cy.get("[data-test-class=\"btn submit\"]").should("not.be.enabled")

    }

    getSaveButton() {
        return cy.get("[data-test-class=\"btn submit\"] > .ui-button-text")
    }

    clickSaveButton() {
        this.getSaveButton().click()
    }

    validateCatalogueCreated(catalogueName) {
        /*  cy.get("[data-test-class=\"td catalogueName\"]").each(($el, index, $list) => {
            var name = $el.text()
            if ($el.text().includes(catalogueName)) {
                expect(name).to.contain.text(catalogueName)
            }
        })*/
        cy.log("catalogue created successfully")
    }

    validateProductsValidation() {
        this.getDialogue().should("have.text", "Please select atleast one Product")
    }

    getClearImage() {
        cy.get("#clear-image").click()
    }

    getDialogue() {
        return cy.get(".p-confirm-dialog-message")
    }

    validateCreatedCatalogue(){
        this.getDialogue().should("be.visible", "Catalogue Created successfully!")
    }

    submitCatalogue() {
        this.getSaveButton().click()
    }

    updateCatalogueDetails() {
        cy.fixture("productDetails").then(function (data) {
            this.data = data
            cy.contains(this.data.ProductName).click()
        })
    }

    getFormField(fieldName) {
        return cy.get(`[name="${fieldName}"]`)
    }

    getLabels(labelName) {
        const labels = {Email: 1, Company: 2, Codes: 3}
        return cy.get(`.pgn__form-group:nth-of-type(${labels[labelName]}) label`)
    }

    navigateToCatalogueScreen() {
        if (cy.get("preloader").should("not.exist")) {
            this.getHouseholdButton().click({force: true})
            // eslint-disable-next-line cypress/no-unnecessary-waiting
            cy.wait(3000)
        }
        if (cy.get("preloader").should("not.exist")) {
            this.getProcurementOption().should("be.visible")
            this.getProcurementOption().click({force: true})
        }
    }


    getHouseholdButton() {
        return cy.get("#navbarDropdownMenuLink-household")
    }

    getProcurementOption() {
        return cy.get("[data-test-class=\"href procurement\"]")
    }

    getSuccessMessage() {
        return cy.get(".alert-dialog .message")
    }


    validateDuplicateCatalogue() {
        this.getDialogue().should("to.be.visible")
            .should("have.text", "Catalog already exists for the given name")
    }

    getEditIcon(catalogueName) {
        return cy.contains(catalogueName).parent("[data-test-class=\"td catalogueName\"]").siblings("[data-test-class=\"td actions\"]").children("[data-test-class=\"btn editCatalogue\"]")
    }

    editCatalogue(catalogueName) {
        this.getEditIcon(catalogueName).click()
    }

    selectCatalogue(catalogueName) {
        return cy.contains(catalogueName).parent("[data-test-class=\"td catalogueName\"]").siblings("[data-test-class=\"td sl-no\"]").children("[data-test-class=\"chkbx catalogue\"]")
    }

    getDeleteButton() {
        return cy.contains("Delete")
    }

    getSelectionCheckBox(CatalogueName) {
        return cy.contains(CatalogueName).parent().parent().children(":nth-child(1)")
    }

    deleteCatalogue(catalogueName) {
        this.selectCatalogue(catalogueName).click()
        this.getDeleteButton().click()
    }

    validateDeletionMessage() {
        this.getDialogue().should("to.be.visible")
            .should("have.text", "Selected record(s) deleted successfully!")
    }


    filterByIndustry(product) {
        cy.get("Filter").click()
        cy.get("formcontrolname=\"externalId\"").type(product)
    }
}

export default CataloguePage
