/// <reference types="cypress" />
class PurchaseOrderPage {

    navigateToPORListing() {
        this.goToPurchaseorderListingScreen().click()
    }

    validatePORAfterReject() {
        cy.url().should("include", "orders-list")
    }

    getSolutionsMenu() {
        return cy.get("#navbarDropdownMenuLink-solutions")
    }

    goToPurchaseorderListingScreen() {
        return cy.waitUntil(() => cy.contains(" PO Requisition Listing "))
    }

    getTableElement() {
        return cy.waitUntil(() => cy.get("table"))
    }

    validateTableHavingData() {
        this.getTableElement().getTable().should(tableData => {

            cy.waitUntil(() => expect(tableData)).not.to.be.empty
        })
    }

    validateTableColumns() {
        cy.get(".ui-table-thead tr th").each(($e1, index, $list) => {
            const text = $e1.text()
            cy.log(text)
            if (text.includes("PO Requisition #")) {
                cy.get(".ui-table-thead tr th").eq(index).should("contain.text", "PO Requisition #")
                cy.log("PO Requisition # Name is available in supplier listing screen")
            }
            if (text.includes("Status")) {
                cy.get(".ui-table-thead tr th").eq(index).should("contain.text", "Status")
                cy.log("PO  Status column is available")
            }
        })
    }

    VerifyPOR(POR) {
        cy.get("tbody").get("td").each(($el, index, $list) => {
            var por = $el.text()
            if (por == POR) {
                cy.wrap($el).click()
            }
        })
    }


    getPrice(price) {
        return cy.contains(price).should("have.text", price)
    }

    validatePrice(price) {

        /* cy.getPrice().contains(productName).then((product) => {
            expect(product).to.contain.text(productName)
        }*/
        this.getPrice(por.price)
    }

    validateTax(por) {
        this.getTax(por.tax)
    }

    getTax(tax) {
        return cy.contains(tax).should("have.text", tax)
    }

    selectPOR() {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        // cy.waitUntil(() => cy.contains(por)).click()
        cy.get("[data-test-class=\"href poRequisition\"]").eq(0).click()
    }

    clickOkButton() {
        cy.waitUntil(() => cy.get(".left-right-margin-2px > .ui-button-text")).click()
    }

    validateCreatedPOR(por) {
        this.getPOR().then((POR) => {
            expect(POR).to.contain(por)
        })
    }

    getBudgetDropdown() {
        return cy.contains("Select Budget")
    }

    chooseBudget(budget) {
        return cy.contains(budget)
    }

    selectBudget(budget) {
        this.getBudgetDropdown().click({force: true})
        this.chooseBudget(budget).click()
    }

    getApproveALLButton() {
        cy.contains("Approve All").click()
    }

    getComments() {
        return cy.get("#comment")
    }

    getConfirmButton() {
        //cy.wait(4000)
        return cy.get(".left-right-margin-2px > .ui-button-text")
    }

    validateConfirmButton() {
        cy.contains("Confirm").should("be.enabled")
    }

    ApprovePOR(comments) {
        this.getApproveALLButton()
        this.getComments().type(comments)
        //this.validateConfirmButton()
        this.getConfirmButton().click()
    }

    getRejectAllButton() {
        cy.contains("Reject All").click()
    }

    rejectPOR(rejectComments) {
        this.getRejectAllButton()
        this.getComments().type(rejectComments)
        this.getConfirmButton().click()
    }


    getPORStatus(por) {
        return cy.contains(por).parent().parent().children()

    }

    getPORToReject(por) {
        return cy.waitUntil(() => cy.contains(por)).click()
    }


    validatePOTheStatus(po) {
        cy.get("table tbody tr td").each(($el, index, $list) => {
            if ($el.text() == "PARTIALLY_RECEIVED") {
                cy.contains(po).parent().parent().children(":nth-child(6)").should("contain.text", "PARTIALLY_RECEIVED")
            } else if ($el.text() == "APPROVED") {
                cy.contains(po).parent().parent().children(":nth-child(6)").should("contain.text", "APPROVED")
            } else if ($el.text() == "RECEIVED") {
                cy.contains(po).parent().parent().children(":nth-child(6)").should("contain.text", "RECEIVED")
            }
        })
    }



    checkPOR() {
        cy.get("[data-test-class=\"td poRequisition\"]").first().click()
        //     .each(($el,index,$list) => {
        //         cy.wrap($el).eq(1).click()
        // })

    }


    validatePORTheStatus(por) {
        cy.get("table tbody tr td").each(($el, index, $list) => {
            if ($el.text() == "PENDING_APPROVAL") {
                cy.contains(por).parent().parent().children(":nth-child(7)").should("contain.text", "PENDING_APPROVAL")
            } else if ($el.text() == "APPROVED") {
                cy.contains(por).parent().parent().children(":nth-child(7)").should("contain.text", "APPROVED")
            } else if ($el.text() == "REJECTED") {
                cy.contains(por).parent().parent().children(":nth-child(7)").should("contain.text", "REJECTED")
            }
        })
    }


    validatePendingApprovalStatus(por) {
        this.getPORStatus(por).should("contain.text", "PENDING_APPROVAL")
    }

    navigateToMarketPalceScreen() {
        this.getSolutionsMenu().click({force: true})
        cy.wait(2000)
        this.getMarketPlace().click({force: true})
        // cy.contains(" Marketplace ").click({force: true})
    }

    getSolutionsMenu() {
        return cy.get("#navbarDropdownMenuLink-solutions")
    }

    getMarketPlace() {
        return cy.get("[data-test-class=\"href marketPlace\"]")
    }

    getCatalogueLink(catalogueName) {
        cy.waitUntil(() => cy.contains(catalogueName)).click()
    }

    getAddToCart() {
        return cy.get("")
    }

    getProducts() {
        this.getAddToCart().click({force: true})
    }

    clickIncreaseQuantity() {
        cy.waitUntil(() => this.increaseQuantity()).click()
    }

    increaseQuantity() {
        return cy.get(".left-right-margin-2px > .ui-button-text")
    }

    clickMyCart() {
        this.getMyCart().click()
        this.getProceed().click()
    }

    getSupplierDropdown() {
        cy.get("#suppliers").click()
        cy.contains("Croma").click()
    }

    getMyCart() {
        return cy.get("#togglercart-button")
    }

    getProceed() {
        return cy.contains("Proceed")
    }

    getDepartment() {
        return cy.get("#department")
    }

    selectDepartment() {
        cy.get("#department > .ui-dropdown > .ui-dropdown-label-container > .ui-dropdown-label").click()
    }

    getCostcenter() {
        cy.get("#costCenter").click()
        cy.get(":nth-child(5) > .ui-dropdown-item").click()
    }

    getDeliveryAddress() {
        return cy.contains("Delivery Address")
    }


    getSupplierDropdown() {
        cy.get("#suppliers").click()
        cy.contains("Croma").click()
    }

    getAddress() {
        return cy.get("#deliveryAddress")
    }

    selectDeliveryAddress() {
        cy.get(":nth-child(1) > .ui-dropdown-item > .ng-star-inserted").click()
    }

    getContractSection() {
        return cy.contains("Contract Selection")
    }

    clickContractSection() {
        this.getContractSection().click()
    }

    getCheckOutButton() {
        return cy.get(".center > .ui-button-text")
    }

    getYesButton() {
        return cy.contains("Yes")
    }

    getSuccessPopup() {
        return cy.get("body > p-dynamicdialog  app-custom-info-dialog > div > b")

    }

    validatePO(POSuccessMessage) {
        this.getSuccessPopup().invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            var PONumber = fullText.match(pattern)
            cy.log(PONumber)
            this.getSuccessPopup().should("have.text", POSuccessMessage + PONumber)
            cy.log("PO created successfully")
        })
    }

    validatePOReject(PORejectMessage) {
        this.getSuccessPopup().invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            var PONumber = fullText.match(pattern)
            cy.log(PONumber)
            this.getSuccessPopup().should("have.text", PORejectMessage)
            cy.log("PO Rejected Successfully")
        })

    }


    clickOKButton() {
        cy.get(".left-right-margin-2px > .ui-button-text").click()
    }

    clickYesButton() {
        this.getYesButton().click()
    }

    clickCheckoutbutton() {
        this.getCheckOutButton().should("not.be.disabled")
        this.getCheckOutButton().click()
    }

    enterDeliveryAddress(address) {
        cy.waitUntil(() => cy.contains("Delivery Address")).scrollIntoView()
        this.getDeliveryAddress().click()
        this.getDepartment().click()
        this.selectDepartment(address.department)
        this.getCostcenter()
        this.getAddress().click()
        this.selectDeliveryAddress()
    }

    selectContract(contract) {
        cy.contains("Select Contract").click({force: true})
        cy.waitUntil(() => cy.contains(contract)).click()
    }

    getSelectQuantity() {
        return cy.get(".ui-spinner-input")
    }

    enterQuantity() {
        this.getSelectQuantity().clear()
        this.getSelectQuantity().type("0")
    }

    ValidateAddTOCartButton() {
        cy.waitUntil(() => cy.contains("Add To Cart")).should("be.disabled")

    }

    validatePOR() {
        cy.get(".ng-star-inserted > div > b").invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            var PORNumber = fullText.match(pattern)
            cy.log(PORNumber)
            this.getSuccessPOR().should("have.text", "PO Requisition Placed Successfully with number POR" + PORNumber)
            cy.log("POR created successfully")
        })
    }


    getPOR() {
        return cy.contains(" PO Requisition Listing ")
    }

    navigateToPOR() {
        cy.waitUntil(() => this.getSolutionsMenu()).click()
        this.getPOR().click()
    }

    getTableElement() {
        return cy.get("table")
    }

    validateTableHavingData() {
        this.getTableElement().getTable().should(tableData => {
            expect(tableData).not.to.be.empty
        })
    }

    validateCreatedPOR(por) {
        cy.get(".ui-table-tbody tr:nth-child(1)  a:nth-child(1)").then((por) => {
            expect(por).to.contain.text(por)
        })
    }

    getPurchaseOrderRequistion(order) {
        cy.contains(order).click()
        /* cy.get("table>tbody>tr").each(function($row,index, $list)  {
            cy.wrap($row).within(function($cellData, $index, $list){
                //cy.log($cellData).text()
                var por =  $cellData.text()
                if( por == order){
                    // eslint-disable-next-line cypress/no-unnecessary-waiting
                    cy.wait(4000)
                    cy.wrap(por).click()
                }

            })
        })     */
    }

}

export default PurchaseOrderPage
