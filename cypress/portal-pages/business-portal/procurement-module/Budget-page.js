import {getNextDay, getTodayDate} from "../../../support/data";

class BudgetPage{

    getHousehold(){
        return    cy.get("#navbarDropdownMenuLink-household")
    }
    getFinance(){
        return cy.get("[data-test-class=\"href financial\"]")
    }

    getBudget(){
        return cy.contains(" Budget ")
    }

    navigateToBudget(){
        this.getHousehold().click()
        this.getFinance().click()
        this.getBudget().click()
    }
    getNewBudget(){
        return cy.get("[data-test-class=\"btn newBudget\"]")
    }
    getBudgetNumber(){
        return cy.get("[data-test-class=\"ip budgetNumber\"]")
    }

    getTitle(){
        return cy.get("[data-test-class=\"ip budgetTitle\"]")
    }
    getAmount(){
        return cy.get("[data-test-class=\"ip budgetAmount\"]")
    }
    getDepartent(){
        return cy.get("[data-test-class=\"drpdwn department\"]")
    }
    selectDepartment(department){
        this.getDepartent().click()
        cy.get("p-dropdownitem li").contains(department).click()
    }
    getStartDate(){
        return cy.get("[data-test-class=\"dt fromDate\"]")
    }
    getEndDate(){
        return cy.get("[data-test-class=\"dt fromDate\"]")
    }

    provideBudgetDetials(budget){
        this.getBudgetNumber().type(budget.number)
        this.getTitle().type(budget.title)
        this.getAmount().type(budget.amount)
        this.selectDepartment(budget.department)
        this.addFromDate()
        cy.wait(2000)
        this.addToDate()
    }
    provideDraftBudgetDetials(budget){
        this.getBudgetNumber().type(budget.draftNumber)
        this.getTitle().type(budget.draftTitle)
        this.getAmount().type(budget.amount)
        this.selectDepartment(budget.department)
        this.addFromDate()
        cy.wait(2000)
        this.addToDate()
    }

    addFromDate() {
        cy.get("[data-test-class=\"dt fromDate\"]").children().children().click({force: true})
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getTodayDate()) {
                cy.wrap($el).dblclick()
            }
        })
    }

    addToDate() {
        cy.get("[data-test-class=\"dt toDate\"]").children().children().click({force: true})
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getTodayDate()) {
                cy.wrap($el).click()
            }
        })
    }

    clickNewBudget() {
        if (cy.get("preloader").should("not.exist"))
        {
            this.getNewBudget().click()
        }
    }

    goToApprovers(){
        cy.contains("Budget Approvers").click()
    }
    goToOwners(){
        cy.contains("Budget Owners").click()
    }
    getBudgetApprover(){
        return cy.get("[data-test-class=\"tb-pnl budgetApprovers\"]")
    }
    getAddApproverButton(){
        return cy.get("[data-test-class=\"btn addApprovers\"]")
    }

    clickBudgetApprovers(){

        this.goToApprovers()
        this.getAddApproverButton().click()
    }

    selectBudgetApprover(approver){
        cy.wait(3000)
        cy.contains(approver).parent().children(":nth-child(1)").children().click()
        cy.get("app-user-selection.ng-star-inserted > .text-right > [label=\"Add\"] > .ui-button-text").click({force: true})
    }

    selectBudgetOwner(owner){
        cy.waitUntil(()  =>cy.contains(owner).parent().children(":nth-child(1)").children()).click()
        cy.get("app-user-selection.ng-star-inserted > .text-right > [label=\"Add\"] > .ui-button-text").click({force: true})
    }
    getValidationMessage(){
        return cy.get(".p-confirm-dialog-message")
    }
    validateTheBudgetCreation(){
        this.getValidationMessage().should("have.text", "Budget title can not be empty")
    }

    updateBudget(budgetName){
        cy.contains(budgetName).parent("[data-test-class=\"td viewBudget\"]").siblings("[data-test-class=\"td actions\"]").children().children("[data-test-class=\"btn editBudget\"]").click()
    }
    validationOnDraft(){
        this.getValidationMessage().should("have.text", "Successfully saved the budget as draft")
    }
    validationOnDelete(){
        this.getValidationMessage().should("have.text", "Selected DRAFT status record(s) are deleted")
    }

    validateDuplicateBudget(){
        this.getValidationMessage().should("have.text", "Budget already exist with the same title")
    }

    getBudgetOwner(){
        return cy.get("[data-test-class=\"tb-pnl budgetOwners\"]")
    }
    getAddOwnerButton(){
        return cy.get("[data-test-class=\"btn addOwners\"]")
    }
    getSubmitButton(){
        return cy.get(".col-md-12 > div > .ui-button-raised > .ui-button-text")
    }
    clickSubmitButton(){
        this.getSubmitButton().click()
    }


    checkContract(title) {
        cy.get("a[class=\"ui-paginator-last ui-paginator-element ui-state-default ui-corner-all\"]").eq(0).click({ force : true })
        cy.wait(2000)
        cy.contains(title).click()

        // cy.waitUntil(() => cy.contains(this.contract1)).click()

    }

    approveContract() {
        cy.get("textarea").type("approved")
        cy.contains("Approve").click()
        cy.waitUntil(() => cy.get(".p-confirm-dialog-message")).should("have.text", "Successfully Approved the task.")
    }

    rejectContract() {
        cy.get("textarea").type("Rejected")
        cy.contains("Reject").click()
        cy.waitUntil(() => cy.get(".p-confirm-dialog-message")).should("have.text", "Successfully Rejected the task.")
    }


    validateTheSubmitButton(){
        this.getSubmitButton().should("be.disabled")
    }

    getSaveAsDraftbutton(){
        return cy.get("[data-test-class=\"btn saveAsDraft\"] > .ui-button-text")
    }
    clickSaveAsDraft(){
        this.getSaveAsDraftbutton().click()
    }


    navigateToBudgetOwners(){
        cy.contains("Budget Owners").click({force: true})
        this.getAddOwnerButton().click()
    }

    getMyTaskOption(){
        cy.contains("My Tasks").click()
    }

    getSettingsIcon(){
        return  cy.get("#navbarDropdownMenuLink-settings")
    }
    goToConfiguration() {
        this.getSettingsIcon().click()
        this.getMyTaskOption()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
    }

    approveBudget(){
        cy.get("textarea").type("approved")
        cy.contains("Approve").click()
    }
    rejectBudget(){
        cy.get("textarea").type("rejected")
        cy.contains("Rejected").click()
    }



    /* checkBudget() {
        cy.get("tbody td").each(($e1, index, $list) => {
            if ($e1.text() == this.budget1) {
                cy.contains(this.budget1).click()
            }
        })
    }*/
}

export default BudgetPage