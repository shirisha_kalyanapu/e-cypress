import {getNextDay} from "../../../support/data"


class ContractsPage {
    let
    contract1;

    getContracts() {
        return cy.contains(" Contract ")
    }

    getSolutions() {

        return cy.waitUntil(() => cy.contains("Solutions"))
    }

    getNewContract() {
        return cy.contains("New Contract")
    }

    clickNewContract() {
        if (cy.get("preloader").should("not.exist")) {
            // eslint-disable-next-line cypress/no-unnecessary-waiting
            cy.wait(3000)
            this.getNewContract().click()
        }

    }

    clickFilterDropdown(status) {
        cy.get("#filter-by").click()
        cy.get(" p-dropdownitem li").contains(status).click()
    }

    navigateToContracts() {
        this.getSolutions().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        this.getContracts().click()
    }

    validateApprovedContractStatus() {
        cy.waitUntil(() => cy.url().should("include", "contracts"))
        cy.get("tbody tr").each(($el, index, $list) => {
            cy.log($el)
            let contract = $el.text()
            cy.log(contract)
            if (contrat == this.contract1) {
                cy.contains(this.contract1).children(cy.getBySel(".ui-table-tbody > :nth-child(1) > :nth-child(7)")).should("have.text", "Future")
            }
        })
    }

    checkApprovedStatus() {
        cy.get("tbody td ")
    }

    validateApprovedContractStatus() {
        cy.waitUntil(() => cy.url()).should("include", "contracts")
        cy.get("tbody tr").each(($el, index, $list) => {
            cy.log($el)
            let contact = $el.text()
            cy.log(contact)
            if (contact == this.contract1) {
                cy.contains(this.contract1).parent().parent().children(":nth-child(7)").should("have.text", "Future")
            }
        })
    }

    navigateToEditContract(contractId) {
        //  cy.contains().parent().children(":nth-child(8)").click()
    }

    getTitle() {

        return cy.get("#title")
    }

    getSelectSupplier() {
        return cy.contains("Select Supplier")
    }

    selectSupplier(supplier) {
        this.getSelectSupplier().click({force: true})
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.waitUntil(() => cy.get("p-dropdownitem li")).contains(supplier).click({force: true})
    }

    getCategoryType() {
        cy.contains("Select Contract Category")
    }

    slectContractCategory(type) {
        cy.waitUntil(() => cy.contains("Select Contract Category")).click({force: true})
        cy.waitUntil(() => cy.get("p-dropdownitem li")).contains(type).click({force: true})
    }

    getSartDate() {
        cy.get("#startDate").click()
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el)
                    .click()
            }
        })
    }

    getEndDate() {
        cy.get("#endDate").click()
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el)
                    .click()
            }
        })
    }

    enterContractDetails(contract) {
        this.getTitle().type(contract.title)
        this.selectSupplier(contract.supplier)
        this.slectContractCategory(contract.type)
        this.getSartDate()
        cy.wait(3000)
        this.getEndDate()
        //this.selectNoticePeriod(contract.noticePeriod)
    }

    enterDraftContractDetails(contract) {
        this.getTitle().type(contract.draftContract)
        this.selectSupplier(contract.supplier)
        this.slectContractCategory(contract.type)
        this.getSartDate()
        cy.wait(3000)
        this.getEndDate()
        //this.selectNoticePeriod(contract.noticePeriod)
    }

    provideDepartment(contract) {
        this.selectDepartment(contract.department)
    }

    provideCostCenter(contract) {
        this.selectCostCenter(contract.costCenter)
    }

    getNoticePeriod() {
        return cy.contains("Select Contract Category")
    }

    selectNoticePeriod(noticePeriod) {
        this.getNoticePeriod().click({force: true})
        cy.get("p-dropdownitem li").contains(noticePeriod).click({force: true})
    }

    selectDepartment(department) {
        cy.contains("Select Department").click({force: true})
        cy.wait(3000)
        cy.get("p-dropdownitem li").contains(department).click({force: true})
        cy.wait(3000)
    }

    selectCostCenter(costcenter) {
        cy.contains("Select Cost Center").click({force: true})
        cy.wait(3000)
        cy.get("p-dropdownitem li").contains(costcenter).click({force: true})
        cy.wait(3000)
    }

    getContactPersonName() {
        return cy.get("#clientPersonName")
    }

    getClientEmail() {
        return cy.get("#clientEmail")
    }

    getClientNumber() {
        return cy.get("#clientPhoneNumber")
    }

    getContactPersonType() {
        return cy.get("#contactPersonType div span")
    }

    selectContactpersonType(type) {
        // eslint-disable-next-line cypress/no-unnecessary-waiting

        cy.waitUntil(() => this.getContactPersonType()).click({force: true})
        cy.get("p-dropdownitem li").contains(type).click({force: true})


    }

    provideClientcontact(contact) {
        cy.contains("Contact Persons").click()
        this.getContactPersonName().type(contact.name)
        this.getClientEmail().type(contact.email)
        this.getClientNumber().type(contact.contact)
        this.selectContactpersonType(contact.contactPersonType)
        cy.contains("Add").click()
    }

    provideSuppliercontact(contact) {
        cy.contains("Contact Persons").click()
        this.getContactPersonName().type(contact.name)
        this.getClientEmail().type(contact.email)
        this.getClientNumber().type(contact.contact)
        this.selectContactpersonType(contact.supplierContactPerson)
        cy.contains("Add").click()

    }

    getAddApproversButton() {
        return cy.contains("Add Approvers").click()

    }

    clickAddApprovers() {
        this.getAddApproversButton().click()
    }

    navigateToContractsInSupplierPortal() {
        cy.waitUntil(() => cy.forceVisit("http://15.207.101.104:8221/"))

        cy.waitUntil(() => cy.get("input[placeholder='Username or Email']").type("shirisha.k@trimindtech.com"))
        cy.get("input[placeholder='Password']").type("test12345")
        cy.contains("Sign In").click()
        cy.contains("Contracts").click()
    }

    selectContract(contract) {
        cy.contains(contract).click()
    }

    addApprovers() {
        cy.contains("Contract Approvers").click()
        cy.contains("Add Approvers").click()


    }

    addSignee() {
        cy.contains("Signee Person Details").click()
        cy.contains("Add Approvers").click()
    }

    selectApprover(approver) {
        cy.waitUntil(() => cy.contains(approver).parent().children(":nth-child(1)").children()).click()
        cy.get("app-user-selection.ng-star-inserted > .text-right > [label=\"Add\"] > .ui-button-text").click({force: true})
    }

    selectContractTemplate(template) {
        cy.contains("Contract Documents").click()
        cy.get("#currency > .ui-dropdown > .ui-dropdown-trigger > .ui-dropdown-trigger-icon").click({force: true})
        // eslint-disable-next-line cypress/no-unnecessary-waiting

        cy.waitUntil(() => cy.get("p-dropdownitem li").contains(template)).click()

    }

    goToSigneePersonSection() {
        cy.contains("Signee Person Details").click()
        cy.contains("Add Signee").click({force: true})
    }

    selectSigneePerson(signee) {
        cy.get("tbody td").contains(signee).parent().children(":nth-child(1)").children().click()

        cy.waitUntil(() => cy.get("app-user-selection.ng-star-inserted > .text-right > [label=\"Add\"] > .ui-button-text")).click({force: true})

    }

    clickSendForReviewButton() {
        cy.contains("Send For Review").click()

    }

    getSuccessPopup() {
        return cy.get(".ng-star-inserted > div > b")
    }

    validateCreatedContracts(successMessage) {
        this.getSuccessPopup().invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            // var pattern = ([A-Za-z0-9]+)$
            let contractId = fullText.match(pattern)
            cy.log(contractId)
            this.getSuccessPopup().should("have.text", successMessage + contractId)
            cy.log()
            cy.log("Contract created successfully!")
            //this.getSuccessPopup().should("have.text", "Invoice Created successfully")
            this.contract1 = "CONTRACT-" + contractId
            cy.log(this.contract1)
            //cy.get(".left-right-margin-2px > .ui-button-text").click()
        })
    }


    validateSubmitForReviewButton() {
        cy.contains("Send For Review").should("be.enabled")
    }

    getMyTaskOption() {
        cy.waitUntil(() => cy.contains("My Tasks")).click()
    }

    getSettingsIcon() {
        return cy.get("#navbarDropdownMenuLink-settings")
    }

    goToConfiguration() {
        this.getSettingsIcon().click()
        this.getMyTaskOption()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
    }

    clickDraftButton() {
        cy.contains("Save As Draft").should("be.enabled")
        cy.contains("Save As Draft").click()
    }

    checkContract() {
        cy.get("a[class=\"ui-paginator-last ui-paginator-element ui-state-default ui-corner-all\"]").eq(0).click({ force : true })
        // cy.get("#ui-tabpanel-6 > .col-md-8 > .ui-table > p-paginator.ng-star-inserted > .ui-paginator-bottom > .ui-paginator-last > .ui-paginator-icon").click()
        cy.wait(2000)
        cy.contains(this.contract1).click()

        // cy.waitUntil(() => cy.contains(this.contract1)).click()

    }

    approveContract() {
        cy.get("textarea").type("approved")
        cy.contains("Approve").click()
        cy.waitUntil(() => cy.get(".p-confirm-dialog-message")).should("have.text", "Successfully Approved the task.")
    }

    rejectContract() {
        cy.get("textarea").type("Rejected")
        cy.contains("Reject").click()
        cy.waitUntil(() => cy.get(".p-confirm-dialog-message")).should("have.text", "Successfully Rejected the task.")
    }

    /*ValidateTheStatusOfContract(contract) {
        if ($e1.text() == this.contract1).each(($e1,){
    })*/

    clickOkButton() {
        cy.get(".left-right-margin-2px > .ui-button-text").click()

    }


    validateDraftedContract() {
        cy.get("app-custom-info-dialog div b").invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            // var pattern = ([A-Za-z0-9]+)$
            var contractId = fullText.match(pattern)
            cy.log(contractId)
            cy.get("app-custom-info-dialog div b").should("have.text", "Contract drafted Successfully CONTRACT-" + contractId)
            cy.log("Contract saved as draft successfully!")
            cy.contains("Ok").click()
            this.contract1 = "CONTRACT-" + contractId
            cy.log(this.contract1)

            //  cy.get(".left-right-margin-2px > .ui-button-text").click()
            cy.wait(3000)
            //this.getSuccessPopup().should("have.text", "Invoice Created successfully")
            // this.invoice1 = "INV"+invoiceId
            // cy.log(this.invoice1)
        })
    }

    validateSubmittedContract() {
        cy.get("app-custom-info-dialog div b").invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            // var pattern = ([A-Za-z0-9]+)$
            var contractId = fullText.match(pattern)
            cy.log(contractId)
            cy.get("app-custom-info-dialog div b").should("have.text", "Contract updated Successfully CONTRACT-" + contractId)
            cy.log("Contracted created as successfully!")
            cy.contains("Ok").click()
        })
    }

    contractDbValidation() {
        //const query = 'select * from ep_contract_service.contract where id=' + id;
        const query = "select * from ep_contract_service.contract where title = \"contract2\""
        cy.log(query)
        cy.task("queryDb", query).then(function (result) {
            cy.log("retrieved the object of the query")
            cy.log(result)
            cy.log("Industry name is ", result[0].name)
            cy.log("Industry Description is ", result[0].description)
            cy.log("Industry External id is ", result[0].external_id)
            cy.log(result.length)
        })
    }

    checkContractId() {
        cy.contains(this.contract1).parent("[data-test-class=\"td contractNumber\"]").siblings("[data-test-class=\"td actions\"]").children("[data-test-class=\"btn editContract\"]").click()
       // cy.contains(this.contract1).parent().parent().children(":nth-child(8)").children("[ptooltip=\"Edit Contract\"]").click()
        //  cy.contains("CONTRACT-220202120108").parent().children(":nth-child(8)").children("button[ptooltip=\"Edit Contract\"]").click()
        /*cy.get("tbody tr").each(($e1, index, $list) => {
            if ($e1.text() === this.contract1) {*/

        //cy.contains(this.invoice1).should("not.be.visible")
        //  }
        /*  else {
                if ($e1.text() != this.contract1) {
                    cy.get("ui-paginator-next ui-paginator-element ui-state-default ui-corner-all").should("be.visible").click()
                    cy.contains(this.invoice1).click()
                }
            }*/
        // })
    }

}

export default ContractsPage