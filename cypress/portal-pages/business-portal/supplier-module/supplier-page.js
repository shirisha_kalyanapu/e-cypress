
/// <reference types="cypress" />
import {getNextDay, getTodayDate} from "../../../support/data"

class SupplierPage {

    navigateToSupplierScreen() {

        cy.waitUntil(()  => this.getSolutionsMenu()).click()
        this.getSupplierSubMenu().click()
        this.getSupplierTab().click()
    }
    getSupplierSubMenu()
    {
        return cy.get("[routerlink=\"/home/supplier\"]")
    }
    getSolutionsMenu()
    {
        return cy.get("#navbarDropdownMenuLink-solutions")
    }


    getDeleteButton(){
        return cy.contains("Delete")
    }

    deleteSupplier(supplier) {
        this.selectSupplierName(supplier)
        this.getDeleteButton().click()

    }

    selectSupplierName(supplier) {
        cy.waitUntil(()  =>this.getSelectionCheckBox(supplier)).get(" p-tablecheckbox > .ui-chkbox > .ui-chkbox-box")
        cy.get("tr:nth-child(1) p-tablecheckbox").click()

    }

    getSelectionCheckBox(supplier) {
        return cy.contains(supplier).parent().parent().children(":nth-child(1)")
    }

    getNameErrorMessage(){
        return cy.get("#name").parent().children()
    }
    getEmptyNameErrorMessage(){
        return cy.get("#name").parent().children().children()
    }


    getSupplierName(){
        return  cy.get("#name")
    }

    enterSupplierName(supplier){
        cy.get("#name").type(supplier)
        cy.get("#name").clear()

    }

    enterName(supplier){
        cy.get("#name").type(supplier)
    }

    enterEmail(supplier){
        this.getEmail().type(supplier.email)
        this.getEmail().clear()

    }

    enterSite(supplier){
        this.getSite().type(supplier.site)
        this.getSite().clear()
    }
    enterUpdatedBusinessAddress(address) {
        this.getBusinessAddress1().clear()
        this.getBusinessAddress1().type(address.updateMailingAddress)
    }
    enterUpdatedBusinessCity(address) {
        this.getBusinessCity().clear()
        this.getBusinessCity().type(address.updateMailingCity)
    }
    enterUpdatedBusinessCountry(address){
        this.getBusinessCountry().clear()
        this.getBusinessCountry().type(address.updateMailingCountry)

    }

    enterUpdatedBusinessPortal(address){
        this.getBusinessPostal().clear()
        this.getBusinessPostal().type(address.updateMailingPotalcode)
    }







    clearSupplierNameField(supplier){
        this.getSupplierName(supplier.name).clear()
    }
    validateDeletedProductDetailsInDB(supplier) {
        const query = "select * from ep_supplier_service.supplier p where name =\"" + supplier + "\";"
        cy.log(query)
        cy.task("queryDb", query)
            .then(function (result) {
                if (result.length == 0) {
                    cy.log("Supplier is not available in DB , Delete action is successful")
                } else {
                    cy.log("retrieved the object of the query")
                    cy.log("Supplier is ", result[0].name)
                    if (result[0].name == supplier) {
                        cy.log("")
                        throw new Error("Supplier is not deleted from DB")
                    }
                }
            })
    }

    getSupplierTab()
    {
        return cy.contains(" Suppliers ")
    }

    getExternalId(){
        return cy.get("#externalId")
    }
    enterExternalId(supplier){
        this.getExternalId().type(supplier.externalId)
    }

    getBsiCode(){
        return cy.get("#bsiCode")
    }

    clickSupplierName(supplier){
        cy.contains(supplier).click()
    }
    validateViewSupplier(){
        cy.url().should("include", "view")
    }

    enterBsiCode(supplier){
        this.getBsiCode().type(supplier.bsiCode)
    }

    getVestigingNumber() {
        return cy.get("#vestigingNumber")
    }

    enterVestigingNumber(supplier){
        this.getVestigingNumber(supplier.vestigingNumber)
    }

    validateSaveButtonDisabled() {
        return cy.get("button[label='Save']").should("be.disabled")
    }

    getMemo()
    {
        return cy.get("#memo")
    }

    navigateToSuppliersListing()
    {
        this.getSupplierTab().click()
    }

    getEditIcon()
    {
        return cy.get("fa-icon[class='ng-fa-icon']")
    }
    getTableElement() {
        return cy.get("table")
    }

    validatePagination() {
        cy.get("tr > :nth-child(1)").should("be.length", 11)
    }

    validateTableColumns() {
        cy.get(".ui-table-thead tr th").each(($e1,index, $list) => {
            const text = $e1.text()
            cy.log(text)
            if ( text.includes("Name")) {
                cy.get(".ui-table-thead tr th").eq(index).should("contain.text", "Name")
                cy.log("Supplier Name is available in supplier listing screen")
            }
            if ( text.includes("Status")) {
                cy.get(".ui-table-thead tr th").eq(index).should("contain.text", "Status")
                cy.log("Supplier Status column is available")
            }
        })

    }

    validateTableHavingData() {
        this.getTableElement().getTable().should(tableData => {
            expect(tableData).not.to.be.empty
        })
    }

    validateTheSupplierCreation() {
        cy.get(".p-confirm-dialog-message").should("have.text", "Supplier Created successfully!")
    }
    getNewProductButton() {
        return cy.contains("New Product")
    }

    getProductCodeField() {
        return cy.get("#productCode")
    }

    getExternalIdField() {
        return cy.get("#externalId")
    }


    getNameField() {
        return cy.get("#name")
    }

    getDescriptionField() {
        return cy.get("#description")
    }

    selectProductGroup(productGroup) {
        cy.contains(productGroup).click()
        cy.get("#productGroup [icon=\"pi pi-angle-right\"] > .ui-button-icon-left").click().catch((err) => {
            cy.console.log("Product groups are not available")
        })
    }

    selectSupplier(supplier) {
        cy.contains(supplier).click()
        cy.get("#department [icon=\"pi pi-angle-right\"] > .ui-button-icon-left").click().catch((err) => {
            cy.console.log("Supplier Data is not available")
        })
    }

    selectGeneralLedger(ledger) {
        cy.get("#GeneralLedgerDTO label").click()
        this.getDropdownOption().contains(ledger).click().catch((err) => {
            cy.console.log("General Ledgers are not available")
        })
    }

    getDropdownOption() {
        return cy.get("p-dropdownitem > li")
    }

    selectCpvCode(code) {
        cy.get("#CpvCodeDTO label").click()
        this.getDropdownOption().contains(code).click()
    }

    selectCostAccount(costaccount) {
        cy.get("#CostAccountDTO  label").click()
        this.getDropdownOption().contains(costaccount).click()
    }

    getCostPrice() {
        return cy.get("#costPrice")
    }

    getSalesPrice() {
        return cy.get("#salesPrice")
    }

    getMarginPrice() {
        return cy.get("#margin")
    }

    getTaxPrice() {
        return cy.get("#taxPercentage")
    }

    addPriceDetails(costprice, salesprice, margin, tax) {
        this.gotoPriceTab()
        this.getCostPrice().type(costprice).catch((err) => {
            cy.console.log("Please provide test data to enter Cost Price")
        })
        this.getSalesPrice().type(salesprice).catch((err) => {
            cy.console.log("Please provide test data to enter Sales Price")
        })
        this.getMarginPrice().type(margin).catch((err) => {
            cy.console.log("Please provide test data to enter MarginPrice")
        })
        this.getTaxPrice().type(tax).catch((err) => {
            cy.console.log("Please provide test data to enter Tax ")
        })
    }

    addEffectiveDate(effectiveDate) {
        cy.get("#effectiveFrom > .ui-calendar > .ui-inputtext").click()
        cy.get("#effectiveFrom  td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == effectiveDate) {
                cy.wrap($el).click()
            }
        })
    }

    addEffectiveTillDate(effectiveDate) {
        cy.get("#effectiveTill > .ui-calendar > .ui-inputtext").click()
        cy.get("#effectiveTill  td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == effectiveDate) {

                cy.wrap($el).click()
            }
        })
    }

    addProductCoverImage(coverImage) {
        cy.contains("Images").click()
        cy.get("p-fileupload.ng-star-inserted > .ui-button > .ng-star-inserted").attachFile(coverImage)

    }

    getProductImages() {
        return cy.get(".ui-fileupload-choose > input")
    }

    addProductImages(images) {
        cy.fixture("productDetails").then(function (data) {
            this.data = data
            cy.get(".ui-fileupload-choose > input").attachFile(this.data.image1)
                .attachFile(this.data.image2)
                .attachFile(this.data.image3)
                .attachFile(this.data.image4)
                .attachFile(this.data.image5)
        })
        this.getUploadButton().click()
    }

    getUploadButton() {
        return cy.get("[icon=\"pi pi-upload\"] > .ui-button > .ui-button-text")
    }

    validateSaveButton() {
        cy.contains("save").should("be.disabled")

    }


    submitProductDetails() {
        this.getSaveButton().click()
    }

    validateProductCreated(productName) {
        cy.get(".ui-table-tbody tr:nth-child(1)  a:nth-child(1)").then((product) => {
            expect(product).to.contain.text(productName)
        })
        cy.log("Product created successfully")
    }

    openNewProductPage() {
        this.getNewProductButton().click()
    }

    addProductName(productName) {
        cy.waitUntil(()  =>this.getNameField()).type(productName)

    }

    addProductCode(productCode) {
        this.getProductCodeField().clear().type(productCode)
    }

    addExternalId(id) {
        this.getExternalIdField().type(id)
    }

    addDescription(description) {
        this.getDescriptionField().type(description)
    }

    getEditIcon(supplier) {
        return cy.contains(supplier).parent().parent().children(":nth-child(7)").children("[ptooltip=\"Edit Supplier\"]")

    }

    openEditSupplierView(supplier) {
        this.getEditIcon(supplier).click()
    }

    getDeleteButton() {
        return cy.contains("Delete")
    }

    deleteProduct(productName) {
        this.selectProduct(productName)
        this.getDeleteButton().click()
    }

    selectProduct(productName) {

        this.getSelectionCheckBox(productName).get(" p-tablecheckbox > .ui-chkbox > .ui-chkbox-box")
        cy.get("tr:nth-child(1) p-tablecheckbox").click()

    }

    getSelectionCheckBox(productName) {
        return cy.contains(productName).parent().parent().children(":nth-child(1)")
    }

    validateDeletedProductDetailsInDB(productName) {
        const query = "select * from ep_product_service.product p where name =\"" + productName + "\";"
        cy.log(query)
        cy.task("queryDb", query)
            .then(function (result) {
                if (result.length == 0) {
                    cy.log("Product is not available in DB , Delete action is successful")
                } else {
                    cy.log("retrieved the object of the query")
                    cy.log("Product name is ", result[0].name)
                    if (result[0].name == productName) {
                        cy.log("")
                        throw new Error("Product is not deleted from DB")
                    }
                }
            })
    }

    getDialogue() {
        return cy.get(".p-confirm-dialog-message")
    }


    getValidationMessage(){
        return cy.get(".p-dialog-content")
    }
    validateDuplicateSupplierDetails() {
        this.getValidationMessage().should("to.be.visible").should("have.text","Supplier already exists for the given name")
    }

    validateUpdatedSupplierName() {
        this.getValidationMessage().should("to.be.visible").should("have.text","Supplier Name should not be edit")
    }

    validateUpdatedSupplierDetails() {
        this.getValidationMessage().should("to.be.visible").should("have.text","Supplier Updated successfully!")
    }

    validateDeletedSupplier() {
        this.getValidationMessage().should("to.be.visible").should("have.text","Selected record(s) deleted successfully!")
    }


    updateDescription(updatedDescription) {
        this.getDescriptionField().clear()
        this.getDescriptionField().type(updatedDescription)
        this.submitProductDetails()
    }

    validateUpdatedDescDetails(productName, updatedDescription) {
        cy.contains(productName).parent().parent().children(":nth-child(5)").should("have.text", updatedDescription)
    }



    getSupplierName(){
        cy.get("#name")
    }

    validateProductCodeRequiredMessage(productCodeRequiredMessage) {
        this.getProductCodeField().clear()
        this.getDescriptionField().click()
        this.getProductCodeField().parent().children(":nth-child(2)").should("have.text",productCodeRequiredMessage)
    }

    validateBusinessAddress(address) {
        if (address.length > 50) {
            this.getBusinessAddress1().type(address)
            this.getBusinessCity().click()
            // this.getBusinessAddress1().parent().children().should("have.text", "Address can not be longer than 50 characters")
            cy.get("#address1").parent().children(":nth-child(2)").children().should("have.text", "Address can not be longer than 50 characters")// cy.get(":nth-child(2) > .col-md-8 > .row > .invalid-feedback > .ng-star-inserted").should("have.text", supplierNameValidationMessage)
        }
        if (address.length == 0) {
            this.addBusinessAddress1(address).clear()
            this.getBusinessAddress1().click()
            this.getBusinessCity().click()
            this.getBusinessAddress1().parent().children().should("have.text", "Enter Address")
        }
    }

    validateBusinessCity()  {
        if (city > 50){
            this.getBusinessCountry().click()
            this.getBusinessCity().parent().children().should("have.text", "Country can not be longer than 50 characters")
        }
    }

    validateBusinessPostalCode (){
        if(country > 50){
            this.getBusinessPostal().click()
            this.getBusinessCountry().parent().children().should("have.text","Postal Code can not be longer than 10 characters")
        }
    }

    validateNameField(name,validationMesage) {
        if (name.length > 50) {
            this.enterName(name)
            this.getEmail().click()
            this.getNameErrorMessage().should("have.text", "Name can not be longer than 50 characters")
            cy.log("validated the name field for 50 characters")
        } else if (name.length == 0) {
            cy.waitUntil(()  =>  this.enterSupplierName()).click()
            this.getSupplierName().click()
            this.getEmail().click()
            this.getNameErrorMessage().should("have.text", "Enter Name")
            cy.log("validated name for empty field")
        }
    }
    validateEmptyNameField(name,validationMesage){
        this.getEmail().click()
        this.getNameErrorMessage().should("have.text", "Enter Name")
        cy.log("validated name for empty field")
    }

    getNameErrorMessage(){
        return cy.get("#name").parent().children(":nth-child(2)")

    }
    getEmailErrorMessage(){
        return cy.get("#email").parent().children(":nth-child(2)")
    }


    validateEmailRequiredErrorMessage(supplierEmailValidationMessage) {
        this.getSite().click()
        this.getEmailErrorMessage().should("have.text", supplierEmailValidationMessage)
    }


    validateSupplierSiteRequiredErrorMessage(supplierSiteValidationMessage) {
        this.getMemo().click()
        this.getSite().parent().children(":nth-child(2)").should("have.text", supplierSiteValidationMessage)
    }


    clearProductField() {
        this.getNameField().click()
        this.getNameField().clear()
    }

    validatePriceErrorMessage(PriceRequiredMessage) {
        this.getPriceValidation().should("have.text",PriceRequiredMessage)
    }
    deletePrice()
    {
        this.getDeleteIcon().click()
    }
    getPriceValidation()
    {
        return cy.get("app-price p")
    }
    getDeleteIcon()
    {
        return cy.get("[ptooltip=\"Delete\"]")
    }

    gotoPriceTab() {
        return  cy.contains("Price").click()
    }

    openNewSupplierPage() {
        cy.contains("New Supplier").click()
    }
    getEmail(){
        return cy.get("#email")
    }
    getSite()
    {
        return cy.get("#site")
    }
    addGeneralDetails(supplier) {
        cy.get("#name").type(supplier.name)
        cy.get("#email").type(supplier.email)
        this.getSite().type(supplier.site)
        this.addFromDate()
        this.addToDate()

    }
    getSupplierName(name)
    {
        cy.get("#name").clear()
        cy.get("#name").click()

    }
    addSupplierName(supplier){
        return  cy.get("#name").type(supplier)
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }
    updateSupplierName(supplier) {
        cy.get("#name").clear()
        cy.get("#name").type(supplier.updatedSupplierName)
    }

    updateSupplierEmail(supplier){
        this.getEmail().clear()
        this.getEmail().type(supplier.updateSupplierEmail)
        
    }

    updateSupplierSite(supplier) {
        this.getSite().clear()
        this.getSite().type(supplier.updateSupplierSite)
    }



    addFromDate() {
        cy.get("#startDate > .ui-calendar > .ui-inputtext").click()
        cy.get("#startDate  td").each(($el, index, $list) => {
            var dateName = $el.text()

            if (dateName == getTodayDate()) {
                cy.wrap($el)
                    .click()
            }
        })
    }
    addToDate() {
        cy.get("#endDate > .ui-calendar > .ui-inputtext").click()
        cy.get("#endDate  td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el)
                    .click()
            }
        })
    }
    // validateSupplierCreated(supplierName) {
    //     cy.get("tbody tr td nth-child(1)").then((supplier) => {
    //         expect(supplier).to.contain.text(supplierName.name)
    //
    //     })
    //     cy.log("Supplier created successfully")
    // }

    


    
    getSaveButton() {
        return cy.get("[label=\"Save\"] > .ui-button-text")
        //return cy.contains("Save").click()
    }
    getBusinessAddress(){
        return cy.contains("Business Address")

    }

    addBusinessAddress(address){
        this.getBusinessAddress1().type(address)
        //his.getBusinessAddress().type(address)
    }
    addAddressDetails(address) {
        this.getBusinessAddress().click()
        this.getBusinessAddress1().type(address.BusinessAddress)
        this.getBusinessCity().type(address.BusinessCity)
        this.getBusinessCountry().type(address.BusinessCountry)
        this.getBusinessPostal().type(address.BusinessPotalcode)

    }

    updateAddressDetails(address) {
        cy.contains("Business Address")
            .click()
        this.enterUpdatedBusinessAddress(address)
        this.enterUpdatedBusinessCity(address)
        this.enterUpdatedBusinessCountry(address)
        this.enterUpdatedBusinessPortal(address)
      /*  this.getBusinessAddress1().type(address.updateMailingAddress)
        this.getBusinessCity().type(address.updateMailingCity)
        this.getBusinessCountry().type(address.updateMailingCountry)
        this.getBusinessPostal().type(address.updateMailingPotalcode)*/

    }

    navigateToSupportDocument(document){
        cy.contains("Documents").click()
    }

    getChooseOption(){
        return cy.get("input[type='file']")
    }

    getUploadButton(){
        return cy.contains("Upload")
    }

    uploadDocument(document){
        this.getChooseOption().attachFile(document.filePath)
        this.getUploadButton().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(4000)
    }

    getBusinessAddress1()
    {
        return cy.get("#address1")
    }
    getBusinessCity()
    {
        return cy.get("#city1")
    }
    getBusinessCountry()
    {
        return cy.get("#country1")
    }
    getBusinessPostal()
    {
        return cy.get("#postalCode1")
    }
    getMailingAddress()
    {
        return cy.get("#address2")
    }
    getMailingCity()
    {
        return cy.get("#city2")
    }
    getMailingCountry()
    {
        return cy.get("#country2")
    }
    getMailingPostal()
    {
        return cy.get("#postalCode2")
    }

    addMailingAddressDetails(address) {
        cy.contains("Mailing Address")
            .click()
        this.getMailingAddress().type(address.MailingAddress)
        this.getMailingCity().type(address.MailingCity)
        this.getMailingCountry().type(address.MailingCountry)
        this.getMailingPostal().type(address.MailingPotalcode)
    }
    getIsPrimary(){
        return cy.get("#isPrimary")
    }

    getContactPersonSection(){
        return cy.contains("Contact Person")
    }
    getContactPersonName(){
        return   cy.get("#name")
    }
    getEmail(){
        return  cy.get("#email")
    }
    getContactNumber(){
        return    cy.get("#phone")
    }
    clickAddButton(){
        cy.get("button[label='Add']").click()
    }

    gotoContactperson() {
        this.getContactPersonSection().click()
    }


    addContactPerson(contact){
        this.getContactPersonSection().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(2000)
        this.getIsPrimary().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(6000)
        // this.getContactPersonName().type(contact.contactName)
        cy.get(":nth-child(2) > #name").type("shirisha")
        cy.get(":nth-child(4) > #email").click().type("shirisha@trimindtech.com")
        cy.get("#phone").type("991223424")
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(6000)
        cy.get("button[label='Add']").should("be.enabled")
        // cy.wait(6000)
        // cy.get("#email").type(contact.contactEmail)
        // cy.get("#email").click().type("john@trimindtech.com")
        //  cy.get("#phone").type(contact.contactNumber)
        //cy.get("#phone").type("991223424")

    }

    updateMailingAddressDetails(address) {
        cy.contains("Mailing Address")
            .click()
        this.getMailingAddress().type(address.updateMailingAddress)
        this.getMailingCity().type(address.updateMailingCity)
        this.getMailingCountry().type(address.updateMailingCountry)
        this.getMailingPostal().type(address.updateMailingPotalcode)
        //this.getAddButton().click()
    }
    clickOnAddProduct()
    {
        cy.contains("Add Product").click()
    }
    addProduct(product){
        cy.contains("Supplier Products").click()
        this.clickOnAddProduct()
        cy.get(".col-3 > .mr-1 > .ui-button-text").click()
        cy.get("table td").each(($el, index, item) => {
            var productName = $el.text()

            if (productName == product.ProductName) {
                cy.get("table td:nth-child(5)").eq(index - 1).click().then(() => {
                    cy.get("table td:nth-child(5) button").eq(index - 1).should("be.disabled")
                })
            }
        })
        cy.get("[label=\"Submit\"]")
            .click()
    }
}

export default SupplierPage
