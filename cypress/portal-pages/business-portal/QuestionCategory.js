import SupplierPage from "./supplier-module/supplier-page";

class QuestionCategory {

    getConfigurations() {
        return cy.get("[data-test-class=\"href configuration\"]")
    }
    getQuestionCategory() {
        return  cy.contains(" Question Category ")
    }

    getEsourcing() {
        return cy.contains("[data-test-class=\"href eSourcing\"]")
    }

    getNewQuestionCategory() {
        return cy.contains("New Question Category")

    }

    getName() {
        return cy.get("#name")
    }

    getCancelButton() {
        return cy.contains("Cancel")
    }

    getSavebutton(){
        return cy.contains("Save")
    }
    clickSaveButton(){
        this.getSavebutton().click()
    }

    getDeleteButton(){
        return cy.contains("Delete")
    }

    navigateToQuestionCategory(){
        this.getConfigurations().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        this.getEsourcing().click()
        this.getQuestionCategory().click()
    }
    clickNewQuestionCategory(){
        this.getNewQuestionCategory().click()
    }
    addQuestionCategory(category){
        this.getName().clear()
        this.getName().type(category)
    }

    clickCancelButton(){
        this.getCancelButton()
    }


}
export default QuestionCategory