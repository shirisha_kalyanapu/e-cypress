//import POReceive from "../../../fixtures/BusinessPortal/ProcurementModule/test_data/POReceive.json"
class POReceive {
    get getPencilIcon()
    {
        return cy.get(":nth-child(8) > .ui-button > .ui-button-text")
    }

    getQuantityReceived(){
        return  cy.get("[type ='number']")
    }

    getActualPrice(){
        return cy.get("[formcontrolname='price']")
    }
    getComments(){
        return cy.get("[formcontrolname='comment']")
    }

    clickSaveButton(){
        cy.contains("Save Changes").click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }
    enterComments(comments){
        this.getComments().type(comments)

    }

    getCancelButton()
    {
        cy.contains("Cancel")
    }
    getPOTextBox() {
        return cy.get("[type='text']")
    
    }

    getClearButton(){
        cy.contains("Clear").click()
    }

    enterPONumber(po) {
        cy.get("[type='text']").type(po)
    }
    enterActualPrice(price){
        cy.get("[formcontrolname='price']").type(price)
    }
    enterQuantityReceived(quantity){
        cy.get("[type ='number']").first().type(quantity)
    }
    enterValidPONumber(po){
        cy.get("[type='text']").type(po)
    }

    clickReceiveButton(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.get("[data-test-class=\"btn receivePo\"]").click({force: true})
        //cy.get(".right-margin-2px > .ui-button-text").click()

    }
    clickSearchButton(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.get("[data-test-class=\"btn search\"]").click({force: true})
    }
    CliclCancelButton(){
        cancelButton().click()
    }
    navigateTOPurchaseorder(){
        cy.contains("Solutions").click({force: true})
        cy.contains(" Marketplace ").click({force: true})
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(4000)
        cy.contains(" Purchase Orders ").click({force: true})
    }
    getPoreceive(){
        return cy.contains(" Purchase Order Receipts ")
    }
    navigateToPoReceive(){
        this.getPoreceive().click()
    }
    getTextBox(){
        cy.get("[type='text']").clear()

    }

    clickPONumber(poNumber){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        // cy.intercept(poNumber).as("page")
        //cy.contains(poNumber).should("be.visible") .click()
        //cy.contains(poNumber).performSomeAsyncAction()
        cy.contains(poNumber).click()
        //cy.wait("@page")
        // cy.get(".right-margin-2px > .ui-button-text").should("be.visible")
    }
    selectPurchaseOrder(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.get("[data-test-class=\"href purchase-order\"]").eq(0).click()
    }
    validatePOStatus(){
        cy.get("[data-test-class=\"href purchase-order\"]").eq(0).parent("[data-test-class=\"td purchase-order\"]").siblings("[data-test-class=\"td status\"]")
            .should("have.text", "APPROVED")
    }

    validatePartiallyReceivedStatus(){
        cy.get("[data-test-class=\"href purchase-order\"]").eq(0).parent("[data-test-class=\"td purchase-order\"]").siblings("[data-test-class=\"td status\"]")
            .should("have.text", "PARTIALLY_RECEIVED")
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }



    validateThePoDetails(po) {
        cy.url().should("include", "poNumber="+po)
    }

    performSomeAsyncAction(){

        
    }
    navigateToMarketPalceScreen(){
        cy.get("#navbarDropdownMenuLink-solutions").click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(2000)
        cy.contains(" Marketplace ").click()
        cy.contains(" Purchase Orders ").click()

    }
    getEditIcon(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(8000)
        return  cy.get("table tbody tr td ").parent().children(":nth-child(8)").children("button[icon=\"pi pi-pencil\"")
    }

    clickEditIcon(){
        this.getEditIcon().click()
    }

    getActualQuantity(){
        return  cy.get("table tbody tr td ").parent().children(":nth-child(3)")
    }

    getReceivedQuantity(){
        return  cy.get("table tbody tr td ").parent().children(":nth-child(4)")

    }

    clickPurchaseorderReceipts(){
        cy.contains(" Purchase Order Receipts ").click()
    }

    getInvalidErrorMessage(){
        return cy.get(".p-confirm-dialog-message")
    }

    ValidateInvalidSearch(){
        this.getInvalidErrorMessage().should("to.be.visible").should("have.text","Invalid PO Number. Please try again with a valid PO Number")

    }
    ValidateEmptySearch(){
        this.getInvalidErrorMessage().should("to.be.visible").should("have.text","Please enter a valid PO number in the search text box")
    }


    getSuccessPopup(){
        return cy.get(".p-dialog-content")
    }

    checkStatus(){
        cy.get("table tbody tr").children(":nth-child(6)").eq(0).should("have.text", "PARTIALLY_RECEIVED" || "RECEIVED")
    }


    validateCreatedPoReceive() {
        this.getSuccessPopup().invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            var poReceive = fullText.match(pattern)
            cy.log("PR"+poReceive)
            const pr = "PR"+poReceive
            cy.get("table tbody tr").children(":nth-child(6)").eq(0).should("have.text", "PARTIALLY_RECEIVED" || "RECEIVED")
            //expect(this.pr).to.eq("one")
            // eslint-disable-next-line cypress/no-unnecessary-waiting
            // cy.wait(4000)
            // this.getSuccessPopup().should("have.text", "Successfully submitted PO Receive request. Use this no PR"+poReceive +"as the reference no.")
            // cy.log("PO Receive Created Successfully!")
            // cy.intercept("orders-list").as("page")
            //cy.wait("@page")
        })
    }
}
export default POReceive