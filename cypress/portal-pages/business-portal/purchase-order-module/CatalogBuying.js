/// <reference types="cypress" />
class CatalogBuying
{
    getImages()
    {

        // eslint-disable-next-line cypress/no-assigning-return-values
        const img = cy.get(".img")
        img.filter("[src]")
            .filter(":visible")
            .should(($imgs) => $imgs.map((i, img) => expect(img.naturalWidth).to.be.greaterThan(0)))
    }

    clickOnCatalogueBuyingLink()
    {
        // eslint-disable-next-line cypress/no-assigning-return-values
        const link =  cy.get("a[href^=\"/home/po-requisition/catalog\"]")
        link.click()
    }
    validateCatalogSupplierName()
    {
        cy.contains("[data-cy=supplier-name]")
    }
    validateCatalogueProducts()
    {
        cy.request("/api/product-service/catalog?page=1&size=100")
        cy.contains("Croma catalogue").click()
        cy.get(":nth-child(1) > .product-row > .co-12 > .mt-2 > .ui-button > .ui-button-text").click()
    }

}
export default CatalogBuying
