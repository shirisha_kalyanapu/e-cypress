import * as url from "url"

class PurchaseOrder {

    getInvoiceButton(){

    }
    getOrders(){
        cy.get("[data-test-class=\"href orders\"]").click()
    }

    getUpdateDeliveryButton(){
        return   cy.get("[data-test-class=\"btn updateDeliveryInformation\"]")
    }

    clickUpdateDeliverybutton(){
        cy.waitUntil(()  =>this.getUpdateDeliveryButton()).click()
    }

    validateUpdateDeliveryButton(){
        this.getUpdateDeliveryButton().should("be.disabled")
    }
    getDeleviryQuantity(){
        // return  cy.get(":nth-child(6) > .form-control")
        return cy.get(".p-datatable-tbody > :nth-child(1) > :nth-child(6)")
    }

    provideDeliveryQuantity(quantity){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        this.getDeleviryQuantity().type(quantity)
    }

    getSelectAction(){
        return cy.get("p-dropdown[formcontrolname=\"action\"]")
    }
    selectReason(action){
        this.getSelectAction().contains(action).click()
    }


    getComments(){
        return cy.get("textarea[formcontrolname='comments']")
    }
    enterComments(comments){
        this.getComments().type(comments)
    }
    clickSelectAction(){
        this.getSelectAction().click()
    }
    selectAction(){
        cy.get("p-dropdownitem li")
    }
    getCreateInvoice(){
        cy.contains("Create Invoice")
    }
    clickCreateInvoiceButton(){
        this.getCreateInvoice().click()
    }
    getSearchButton(){
        cy.contains("Search")
    }
    clickSaveButton(){
        cy.contains("Save Changes").click()
    }
    clickLogout(){
        cy.get("[data-test-class='href profile']").click()
        cy.get("[data-test-class='href logOut']").click({force: true})
    }
    validateDashboard(){
        cy.url().should("include", "dashboard")
    }
    validatePurchaseOrder(){
        cy.url().should("include", "/orders/purchase-orders")
    }
    checkPurchaseorder(purchaseOrder){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.contains(purchaseOrder).click()
    }
    validateThePoDeliveryScreen(purchaseOrder){
        cy.url().should("include", purchaseOrder)
    }
    clickFilterBy(){
        cy.get(".p-dropdown-trigger").click()
    }
    selectStatus(status){
        cy.get("p-dropdownitem li").contains(status).click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }
    selectPartiallyDeliveredStatus(status){
        cy.get("p-dropdownitem li").contains(status).click()

    }
    selectDeliveryStatus(Delivered){
        cy.get("p-dropdownitem li").contains(Delivered).click()

    }


    clickSearchButton(){
        this.getSearchButton().click()
    }
    navigateToOrders(){
        if (cy.get("preloader").should("not.exist")) {
            cy.contains("Orders").click()
            // this.getOrders().click()
        }
    }
    naviagteToDeliveryscreen(){
        if (cy.get("preloader").should("not.exist")) {
            cy.contains(" Order Delivery ").click()
            // cy.url().should("include", "/orders/delivery")
        }
    }
    getSearchBox(invalidPo){
        cy.waitUntil(()  => cy.get("input[type=\"text\"]")).type(invalidPo)
    }
    clickSearchBox(){
        cy.get(".btn-primary").click()
    }
    validateInvalidateSearch(){
        cy.get(".p-confirm-dialog-message").should("have.text", "Invalid PO Number. Please try again with a valid PO Number")

    }

    getClearButton(){
        cy.contains("Clear")
    }
    clickClearButton(){
        this.getClearButton()
    }
    get
}
export default PurchaseOrder