import "cypress-wait-until"
import {getNextDay} from "../../../support/data"

/// <reference types="cypress" />
class ProductsPage {
    inentoryDropdown = "Inventory";
    productMenu = "Product";

    navigateToProductScreen() {
        cy.get("[data-test-class=\"href inventory\"]").click()
        cy.wait(3000)
        cy.contains("Product").click()

    }

    getTableElement() {
        return cy.get("table")
    }

    validatePagination() {
        cy.get("tr > :nth-child(1)").should("be.length", 11)
    }

    validateTableColumns() {
        cy.get("thead >tr>th:nth-child(1)").should("contain.text", "SI No")
        cy.get("thead >tr>th:nth-child(2)").should("contain.text", "Product Code")
        cy.get("thead >tr>th:nth-child(3)").should("contain.text", "External ID")
        cy.get("thead >tr>th:nth-child(4)").should("contain.text", "Name")
        cy.get("thead th:nth-child(5)").should("contain.text", "Description")
        cy.get("thead th:nth-child(6)").should("contain.text", "CPV Code")
        cy.get("thead >tr>th:nth-child(8)").should("contain.text", "Actions")
    }

    validateTableHavingData() {
        this.getTableElement().getTable().should(tableData => {
            expect(tableData).not.to.be.empty
        })
    }


    getNewProductButton() {
        return cy.contains("New Product")
    }

    getProductCodeField() {
        return cy.get("#productCode")
    }

    getExternalIdField() {
        return cy.get("#externalId")
    }

    getNameField() {
        return cy.get("#name")
    }

    getDescriptionField() {
        return cy.get("#description")
    }

    selectProductGroup(ExtProductGroup) {
        cy.contains(ExtProductGroup).click()
        cy.get("[icon=\"pi pi-angle-right\"]").click()
    }

    selectSupplier(supplier) {
        cy.contains(supplier).click()
        cy.get("#department [icon=\"pi pi-angle-right\"] > .ui-button-icon-left").click()
    }

    selectGeneralLedger(ledger) {
        cy.get("#GeneralLedgerDTO label").click()
        this.getDropdownOption().contains(ledger).click()
    }

    getDropdownOption() {
        return cy.get("p-dropdownitem > li")
    }

    selectCpvCode(code) {
        cy.get(".p-dropdown-trigger-icon").click()
        this.getDropdownOption().contains(code).click()
    }

    selectCostAccount(costaccount) {
        cy.get("#CostAccountDTO  label").click()
        this.getDropdownOption().contains(costaccount).click()
    }

    getCostPrice() {
        return cy.get("#costPrice")
    }

    getSalesPrice() {
        return cy.get("#salesPrice")
    }

    getMarginPrice() {
        return cy.get("#margin")
    }

    getTaxPrice() {
        return cy.get("#taxPercentage")
    }

    addPriceDetails(costprice, salesprice, margin, tax) {
        this.gotoPriceTab()
        this.getCostPrice().type(costprice)
        this.getSalesPrice().type(salesprice)
        this.getMarginPrice().type(margin)
        this.getTaxPrice().type(tax)
    }

    addEffectiveDate() {
        cy.get("#effectiveFrom > .ui-calendar > .ui-inputtext").click()
        /* cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el)
                    .click()
            }
        })*/
    }

    addEffectiveTillDate() {
        cy.get("#effectiveTill > .ui-calendar > .ui-inputtext").click()
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el)
                    .click()
            }
        })
    }


    addProductCoverImage() {
       const filePath = "business-portal/procurement-module/images/coverimage.jpg"
        cy.contains("Images").click()
        cy.wait(3000)
        cy.get(".p-fileupload > .p-button > input").attachFile(filePath)


    }

    getProductImages() {
        return cy.get(".ui-fileupload-choose > input")
    }

    addProductImages(images) {
        cy.contains("Images").click()
        cy.get(".p-fileupload-choose > input").attachFile(images.image1)
            .attachFile(images.image2)
            .attachFile(images.image3)
            .attachFile(images.image4)
            .attachFile(images.image5)
        this.getUploadButton().click()
    }

    getUploadButton() {
        return cy.contains("Upload")
    }

    getSaveButton() {
        return cy.get("[data-test-class=\"btn save\"]")
    }

    clickReorderPoint(){
        cy.contains("Reorder Point").click()
    }
    provideReorderDetails(rop){
        cy.get("[data-test-class=\"ip leadTime\"]").type(rop.leadTime)
        cy.get("[data-test-class=\"ip safetyStock\"]").type(rop.safetyStock)
        cy.get("[data-test-class=\"ip basicStock\"]").type(rop.basicStock)
        cy.get("[data-test-class=\"ip unitSalesPerDay\"]").type(rop.unitSales)
    }
    clickProductAttributes(){
        cy.contains("Product Attributes").click()
    }

    provideProductAttributes(product){
        cy.get(".col-11 > #name").click({force: true})
        cy.get(".col-11 > #name").type(product.name)
        cy.get("#value").click({force: true})
        cy.get("#value").type(product.value)
    }

    submitProductDetails() {
        this.getSaveButton().click({force: true})
    }

    validateProductCreated(productName) {
        cy.get(".p-confirm-dialog-message").should("have.text", "Product Created successfully!")
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        /*cy.wait(2000)
        cy.get(".ui-table-tbody tr:nth-child(1)  a:nth-child(1)").then((product) => {
            expect(product).to.contain.text(productName)
        })*/
        cy.log("Product created successfully")
    }

    openNewProductPage() {
        this.getNewProductButton().click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(1000)
    }

    addProductName(productName) {
        this.getNameField().type(productName)

    }

    addProductCode(productCode) {
        this.getProductCodeField().clear().type(productCode)
    }

    addExternalId(id) {
        this.getExternalIdField().type(id)
    }

    addDescription(description) {
        this.getDescriptionField().type(description)
    }

    getEditIcon(productName) {
        return cy.contains(productName).parent("[data-test-class=\"td viewProduct\"]").siblings("[data-test-class=\"td actions\"]").children().children("[data-test-class=\"btn editProduct\"]")
    }

    openEditProductView(productName) {
        this.getEditIcon(productName).click()
    }

    getDeleteButton() {
        return cy.contains("Delete")
    }

    deleteProduct(productName) {
        this.selectProduct(productName).click({force: true})
        this.getDeleteButton().click()
        cy.wait(3000)
        cy.get(".p-confirm-dialog-accept > .p-button-icon").click()
    }

    selectProduct(productName) {
        return cy.contains(productName).parent("[data-test-class=\"td viewProduct\"]").siblings("[data-test-class=\"td product\"]").children("[data-test-class=\"tbl-chkbx product\"]").children()
        // this.getSelectionCheckBox(productName).get(" p-tablecheckbox > .ui-chkbox > .ui-chkbox-box")
        // cy.get("tr:nth-child(1) p-tablecheckbox").click()

    }

    getSelectionCheckBox(productName) {
        return cy.contains(productName).parent().parent().children(":nth-child(1)")
    }

    validateDeletedProductDetailsInDB(productName) {
        const query = "select * from ep_product_service.product p where name =\"" + productName + "\";"
        cy.log(query)
        cy.task("queryDb", query)
            .then(function (result) {
                if (result.length == 0) {
                    cy.log("Product is not available in DB , Delete action is successful")
                } else {
                    cy.log("retrieved the object of the query")
                    cy.log("Product name is ", result[0].name)
                    if (result[0].name == productName) {
                        cy.log("")
                        throw new Error("Product is not deleted from DB")
                    }
                }
            })
    }

    getDialogue() {
        return cy.get(".p-confirm-dialog-message")
    }

    validateDuplicateProductDetails() {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(200)
        this.getDialogue().should("to.be.visible")
    }

    updateDescription(updatedDescription) {
        this.getDescriptionField().clear()
        this.getDescriptionField().type(updatedDescription)
        this.submitProductDetails()

    }

    validateUpdatedDescDetails(productName, updatedDescription) {
        cy.get(".p-confirm-dialog-message").should("have.text", "Product Updated successfully!")
        //cy.contains(productName).parent().parent().children(":nth-child(5)").should("have.text", updatedDescription)
    }

    validateSaveButtonDisabled() {
        this.getSaveButton().should("be.disabled")
    }

    validateProductCodeRequiredMessage(productCodeRequiredMessage) {
        this.getProductCodeField().clear()
        this.getDescriptionField().click()
        this.getProductCodeField().parent().children(":nth-child(2)").should("have.text", productCodeRequiredMessage)
    }

    validateProductNameErrorMessage(productNameValidationMessage) {
        this.getDescriptionField().click()
        this.getNameField().parent().children(":nth-child(2)").should("have.text", productNameValidationMessage)
    }


    clearProductField() {
        this.getNameField().click()
        this.getNameField().clear()
    }

    validatePriceErrorMessage(PriceRequiredMessage) {
        this.getSaveButton().should("not.be.enabled")
    }

    deletePrice() {
        this.getDeleteIcon().click()
    }

    getPriceValidation() {
        return cy.get("app-price p")
    }

    getDeleteIcon() {
        return cy.get("[ptooltip=\"Delete\"]")
    }

    gotoPriceTab() {
        return cy.contains("Price").click()
    }

    createProduct(productDetails) {
        this.openNewProductPage()
        this.addProductCode(productDetails.ProductCode)
        this.addExternalId(productDetails.ExternalId)
        this.addProductName(productDetails.ProductName)
        cy.get("[data-test-class=\"ip variantTag\"]").type(productDetails.varient)
        this.addDescription(productDetails.Description)
        this.selectProductGroup(productDetails.ExtProductGroup)
        this.selectCpvCode(productDetails.CPVCode)
        this.addPriceDetails(productDetails.CostPrice, productDetails.SalesPrice, productDetails.Margin, productDetails.Tax)
        cy.wait(3000)
        this.addProductCoverImage()
        this.addProductImages(productDetails)
    }

    clickSubmit(){
        this.submitProductDetails()
    }
}

export default ProductsPage
