import PurchaseOrder from "./purchase-order"
import {getNextDay} from "../../../support/data"

class InvoicePage {


    

    let
    invoice1;
    let
    invoiceNumber;
    invoiceNumber = this.invoice1
    getCreateInvoiceButton(po){
        cy.contains(po).parent("[data-test-class=\"td purchaseOrder\"]").siblings("[data-test-class=\"td actions\"]").children("[data-test-class=\"btn createInvoice\"]").children().click()
    }

    navigateToInvoiceScreen() {
        cy.waitUntil(() => this.getSolutionsMenu()).click()
        this.getInvoice().click()
    }

    getTableElement() {
        return cy.waitUntil(() => cy.get("table"))
    }

    validateTableHavingData() {
        this.getTableElement().getTable().should(tableData => {
            cy.waitUntil(() => expect(tableData)).not.to.be.empty
        })
    }

    getInvoice() {
        return cy.get(":nth-child(5) > .nav-link")
    }
    clickInvoice(){
        this.getInvoice().click()
    }

    getSolutionsMenu() {
        return cy.get("#navbarDropdownMenuLink-solutions")
    }

    getNewInvoice() {
        return cy.contains("New Invoice")
    }

    clickNewInvoice() {
        this.getNewInvoice().click()
    }

    getInvoiceType() {
        // return cy.get(" p-dropdown[placeholder='Select Invoice Type']")
        return cy.get("#invoiceTypeId > .width100 > .p-dropdown-trigger > .p-dropdown-trigger-icon")
    }

    getCurrency() {
        return cy.get(" p-dropdown[placeholder='Select Currency']")
    }

    getSelectPOnumber() {
        return cy.get("p-autocomplete[placeholder='Select PO Number']")
    }

    getSelectPaymentTerm() {
        return cy.get("#paymentTerms > .width100 > .p-dropdown-trigger > .p-dropdown-trigger-icon")
    }

    clickEditIcon(){
        cy.get("[data-test-class=\"btn editInvoiceDetails\"]").click()
    }

    getInvoiceLineItems() {
        return cy.contains("Invoice Details")
    }

    getGeneralLedger() {
        return cy.get("p-dropdown[id='glName']")
    }

    selectGeneralLedger(generalLedger) {
        this.getGeneralLedger().children().children(":nth-child(4)").click()
        cy.contains(generalLedger).click()
    }

    getProductName() {
        return cy.get("p-dropdown[id='productName']")

    }

    selectProduct(product) {
        this.getProductName().children().children(":nth-child(4)").click()
        cy.contains(product).click()
    }

    getInvoiceLineItemsAddButton() {
        cy.contains("Select Product").parent().parent().parent().parent().children(":nth-child(3)").children(":nth-child(1)").click()
    }

    getClearButton() {
        return cy.contains("Clear")
    }

    clickClearButton() {
        this.getClearButton().click()
    }

    getInvoiceQuantity() {
        return cy.get("#quantityInvoiced")
    }

    getPrice() {
        return cy.get("#unitSellingPrice")
    }

    getTotalTax() {
        return cy.get("#totalTax")
    }

    enterTax(tax) {
        this.getTotalTax().clear()
        this.getTotalTax().type(tax)
    }

    adddate() {
        cy.get("#invoiceDate").click()
        cy.get("tbody tr td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == getNextDay()) {
                cy.wrap($el)
                    .click()
            }
        })
    }

    validateSelectedLineItem() {
        cy.get("table tbody tr td").each(($e1, index, $list) => {
            var productName = $e1.text()
            cy.get("table tbody tr td").should("have.text", productName)
            // if(product == productName)

        })
    }

    clickAddNonProductLine() {
        return cy.contains("Add Non-product Line")
    }

    enterReason(reason) {
        cy.get("#reason").type(reason)
    }

    enterInvoiceAmount(amount) {
        cy.get("#invoiceAmount").type(amount)
    }

    enterInvoiceLineItems(lineItems) {
        this.clickInvoiceDetails()
        this.clickEditIcon()
        this.enterTax(lineItems.tax)
        // this.getInvoiceLineItemsAddButton()
        cy.contains("Update").click({force: true})
    }

    enterNonProductLineItems(nonProductLineItems) {
        this.clickInvoiceDetails()
        this.clickAddNonProductLine().click({force: true})
        this.selectItem(nonProductLineItems.item)
        this.getReason().type(nonProductLineItems.reason)
        this.getInvoiceAmount().type(nonProductLineItems.amount)
        cy.get(".form-horizontal > .text-right > .btn-primary > .p-button-label").click({force: true})
    }

    getItemDropdown() {
        //return cy.get("p-dropdown[id='productId']").children().children(":nth-child(4)")
        return cy.get("#nonProductId > .p-dropdown > .p-dropdown-trigger")
    }

    selectItem(item) {
        this.getItemDropdown().click({force: true})
        cy.contains(item).click({force: true})
    }

    getReason() {
        return cy.get("#reason")
    }

    getInvoiceAmount() {
        return cy.get("#invoiceAmount")
    }

    getInvoiceApprovers() {
        return cy.get("#ui-tabpanel-2-label")
    }

    getAddApproversButton() {
        return cy.contains("Add Approvers")
    }

    clickApproversTab() {
        this.getInvoiceApprovers().click()
    }

    addApprover() {
        cy.get(":nth-child(1) > [data-test-class=\"td sl-no\"] > p-tablecheckbox > .p-checkbox > .p-checkbox-box").click()
        cy.get("app-invoice-approvers-details button[label='Add']").click({force: true})
        /*cy.wait(3000)
        cy.contains(approver).parent().children(":nth-child(1)").children("[data-test-class=\"tbl-chkbx approver\"]").click()
      */
    }

    clickSupportingDocuments() {
        cy.contains("Supporting Documents").click()
    }

    getApproverAddButton() {

        return cy.contains("Add")
    }

    getChooseOption() {
        cy.get("input[type='file']")
    }

    uploadSupportingDocument(filePath) {
        const path = filePath
        this.getSupportingDocuments().click()
        cy.get("input[type='file']").attachFile(path)
        cy.contains("Upload").click()

    }

    getSupportingDocuments() {
        return cy.contains("Supporting Documents")
    }

    getSubmitbutton() {
        return cy.contains("Submit")
    }

    clickSubmitButton() {
        this.getSubmitbutton.click()
    }

    getDropdownOption() {
        return cy.get("input[placeholder='Select Invoice Type']").parent().children().children()
    }

    selectInvoiceType(type) {
        //this.getInvoiceType().children().children(":nth-child(4)").click({force: true})
        this.getInvoiceType().click({force: true})
        cy.waitUntil(() => cy.contains(type)).click({force: true})

    }

    SelectCurrency(Currency) {
        //this.getCurrency().children().children(":nth-child(4)").click({force: true})
        cy.get("#currency > .width100 > .p-dropdown-trigger").click({force: true})
        cy.contains(Currency).click({force: true})
    }

    selectPaymentTerm(paymentTerm) {
        this.getSelectPaymentTerm().click({force: true})
        cy.contains(paymentTerm).click({force: true})

    }

    selectPoNumber(po) {
        this.getSelectPOnumber().type(po)
        /* cy.get("#pr_id_1_list").each(($e1, index, $list) => {
            var PoNumber = $e1.text()
            if (PoNumber == po) {
                cy.wait(3000)
                cy.wrap($e1).click({force: true})
            }
        })*/
    }

    getMyTaskOption() {
        cy.contains("My Tasks").click()
    }

    getSettingsIcon() {
        return cy.get("#navbarDropdownMenuLink-settings")
    }

    goToConfiguration() {
        this.getSettingsIcon().click()
        this.getMyTaskOption()
    }

    getAddButton() {
        return cy.contains("Add")
    }

    clickAddButton() {
        this.getAddButton().click({force: true})
    }

    getSaveButton() {
        return cy.contains("Submit")
    }

    clickSaveButton() {
        this.getSaveButton().click()
    }

    getSuccessPopup() {
        return cy.get(".ng-star-inserted > div > b")
    }

    validateCreatedInvoice(successMessage) {
        this.getSuccessPopup().invoke("text").then((text) => {
            var fullText = text
            var pattern = /[0-9]+/g
            // var pattern = ([A-Za-z0-9]+)$
            let invoiceId = fullText.match(pattern)
            cy.log(invoiceId)
            this.getSuccessPopup().should("have.text", successMessage + invoiceId)
            cy.log()
            cy.log("Invoice created successfully!")
            //this.getSuccessPopup().should("have.text", "Invoice Created successfully")
            this.invoice1 = "INV" + invoiceId
            cy.log(this.invoice1)
        })
    }

    validatedAlreadyCreatedInvoice(validationMessage){
        cy.get("app-custom-info-dialog div b").should("have.text", validationMessage)
        cy.get(".left-right-margin-2px").click()
    }

    checkInvoiceId() {
        cy.get("tbody tr").each(($e1, index, $list) => {
            if ($e1.text() === this.invoice1) {
                cy.contains(this.invoice1).click()
                //cy.contains(this.invoice1).should("not.be.visible")
            } else {
                if ($e1.text() != this.invoice1) {
                    cy.get("ui-paginator-next ui-paginator-element ui-state-default ui-corner-all").should("be.visible").click()
                    cy.contains(this.invoice1).click()
                }
            }
        })
    }


    checkInvoice() {
        cy.get("tbody tr").each(($e1, index, $list) => {
            if ($e1.text() == this.invoice1) {
                cy.contains(this.invoice1).click()
            } else {
                if ($e1.text() != this.invoice1) {

                    cy.waitUntil(() => cy.get("#ui-tabpanel-3 > .col-md-8 > .ui-table > p-paginator.ng-star-inserted > .ui-paginator-bottom > .ui-paginator-pages > :nth-child(2)")).click()
                    cy.contains(this.invoice1).click()
                }
            }
        })
    }


    approveInvoice() {
        cy.get("textarea").type("approved")
        cy.contains("Approve").click()
    }

    rejectInvoice() {
        cy.get("textarea").type("rejected")
        cy.contains("Rejected").click()
    }

    validateApprovedInvoiceStatus() {

        cy.waitUntil(() => cy.url().should("include", "invoices"))
        cy.get("tbody tr").each(($el, index, $list) => {
            cy.log($el)
            let invoice = $el.text()
            cy.log(invoice)
            if (invoice == this.invoice1) {
                cy.contains(this.invoice1).children(cy.getBySel("td status")).should("have.text", "APPROVED")
            }
        })
    }

    validateRejectedInvoiceStatus() {
        cy.waitUntil(() => cy.url().should("include", "invoices"))
        cy.get("tbody tr").each(($el, index, $list) => {
            cy.log($el)
            let invoice = $el.text()
            cy.log(invoice)
            if (invoice == this.invoice1) {
                cy.contains(this.invoice1).children(cy.getBySel("td status")).should("have.text", "REJECTED")
            }
        })
    }

    clickOkButton() {
        cy.get(".left-right-margin-2px > .p-button-icon").click()
    }

    clickInvoiceDetails() {
        this.getInvoiceLineItems().click({force: true})
    }

    invoiceDate(effectiveDate) {
        var todaysDate = moment().format("DD")
        var formatedDate = todaysDate.replace(/^0+/, "")
        cy.get("#effectiveFrom > .ui-calendar > .ui-inputtext").click()
        cy.get("#effectiveFrom  td").each(($el, index, $list) => {
            var dateName = $el.text()
            if (dateName == formatedDate) {
                cy.wrap($el).click()
            }
        })
    }

    enterGeneralDetails(general) {
        this.selectInvoiceType(general.type)
        this.SelectCurrency(general.currency)
        this.adddate()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }

    selectReceivedPO(po) {
        this.selectPoNumber(po)
    }

    providePaymentTerm(paymentTerm) {
        this.selectPaymentTerm(paymentTerm)
    }

    enterGeneralSectionDetails(general) {
        this.selectInvoiceType(general.type)
        this.SelectCurrency(general.currency)
        this.adddate()
        this.selectPoNumber(general.approvedPo)

    }

    enterInvoiceDetials() {
        this.getInvoiceLineItems()
    }

    getInvoiceApprovers() {
        return cy.contains("Invoice Approvers")
    }

    getAddApproverButton() {
        return cy.contains("Add Approvers")
    }

    clickInvoiceApprovers() {
        this.getInvoiceApprovers().click({force: true})
        this.getAddApproverButton().click({force: true})

    }


    validateAlreadyCreatedInvocie() {
        cy.get("app-custom-info-dialog div b").should("have.text", validationMessage)
        cy.contains("Ok").click()
    }

    clickOkButton() {
        return cy.get(".left-right-margin-2px > .p-button-icon").click()
    }

    getPoValidation() {
        return cy.get("app-custom-info-dialog").children().children(":nth-child(1)")

    }

    validatePOToCreateInvoice(poValidation) {

        cy.waitUntil(()  => this.getPoValidation().invoke("text")).then((text) => {
            this.getPoValidation().should("have.have", poValidation)
            cy.log("Selected Purchase order is not received")

        })
    }
    clickLogout(){
        cy.get("[data-test-class=\"href profile\"]").click()
        cy.get("[data-test-class=\"href logOut\"]").click({force: true})
    }
    
}

export default InvoicePage