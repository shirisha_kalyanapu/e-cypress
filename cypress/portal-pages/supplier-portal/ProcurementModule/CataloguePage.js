/// <reference types="cypress" />
import { getRandomID } from "../../../support/data"

class CataloguePage {

    addCatalogueName(catalogue) {
        // const name = catalogue + getRandomID()
        this.getCatalogueName().type(catalogue)
    }

    getNewCatalogue() {
        return cy.contains("New Catalogue")
    }

    getCatalogueDescription() {
        return cy.get("#description")
    }

    addCatalogueDescription(desc) {
        this.getCatalogueDescription()
            .clear()
            .type(desc)
    }
    validateUpdatedDescDetails(catalogName,updatedDescription)
    {
        cy.contains(catalogName).parent().parent().children(":nth-child(3)").should("have.text", updatedDescription)
    }
    getCatalogueName() {
        return cy.get("#name")
    }

    getNameErrorMessage() {
        return cy.get(".invalid-feedback > .ng-star-inserted")
    }

    validateCatalogueName() {
        // this.getCatalogueName().should('have.length', 25)
        this.getCatalogueDescription().click()
        cy.get(".invalid-feedback > .ng-star-inserted").should("have.text", "name can not be longer than 25 characters")
    }

    validateDescription() {
        // this.getCatalogueName().should('have.length', 25)
        this.getCatalogueName().click()
        cy.get(".invalid-feedback > .ng-star-inserted").should("have.text", "Description can not be longer than 256 characters")
    }

    getCatalogueType() {
        cy.get(".ui-dropdown-label-container > .ng-tns-c13-6").click()
        return cy.get("p-dropdownitem > li > span")
    }

    addCatalogueType(type) {
        cy.get("#catalogType").click()
        cy.get("p-dropdownitem > li > span").each(($el, index, $list) => {
            var catalogueType = $el.text()
            if (catalogueType == type) {
                cy.waitUntil(()  =>  cy.wrap($el)).click()
            }
        })
    }

    attachCoverImage(img) {
        this.getCoverImageChooseButton().attachFile(img)
    }

    getCoverImageChooseButton() {
        return cy.get(".p-fileupload > .p-button > input")
    }

    createCatalogue() {
        cy.fixture("catalogueDetails").then(function (data) {
            this.data = data
            cy.contains("New Catalogue").click()
            cy.get("#name").type(this.data.CatalogueName)
            cy.get("#description").type(this.data.Description)
            this.addCatalogueType(this.data.catalogueType)
            cy.get(".ui-button > .ng-star-inserted").attachFile(this.data.image1)

            cy.get("[label=\"Submit\"] > .ui-button-text").click()
            cy.get("[label=\"Save\"] > .ui-button-text").click()

            this.getDialogue().should("have.text", "Catalog already exists for the given name")


        })
    }

    clickOnAddProduct()
    {
        cy.contains("Add Product").click()
    }
    addProduct(product){
        this.clickOnAddProduct()
        cy.get("[data-test-class=\"btn search\"]").click()
        cy.get("table td").each(($el, index, item) => {
            var productName = $el.text()
            if (productName == product) {
                cy.get("[data-test-class=\"td actions\"]").eq(index - 1).click().then(() => {
                    /* // cy.get("table td:nth-child(5) button").eq(index - 1).should("be.disabled")
                    cy.get("[data-test-class=\"td actions\"]")*/
                })
            }
        })
    }

    clickSubmitButton(){
        cy.get("[label=\"Submit\"]")
            .click()
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
    }
    clickSaveButton(){
        this.getSaveButton().click({force: true})
    }
    getSaveButton() {
        return cy.get("[data-test-class=\"btn save\"]")
    }
    validateCatalogueCreated(catalogueName) {
        this.getDialogue().should("have.text","Catalogue Created successfully!")
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)

        /* cy.get(".ui-table-tbody tr:nth-child(1)  a:nth-child(1)").then((product) => {
            expect(product).to.contain.text(catalogueName)
        })*/
        cy.log("catalogue created successfully")
    }
    validateProductsValidation() {
        //this.getSaveButton().parent().should("be.disabled")
        this.getDialogue().should("have.text", "Please select atleast one Product")
    }
    getClearImage()
    {
        cy.get("#clear-image").click()
    }

    getDialogue() {
        return cy.get(".p-confirm-dialog-message")
    }

    submitCatalogue() {
        this.getSaveButton().click()
    }

    updateCatalogueDetails() {
        cy.fixture("productDetails").then(function (data) {
            this.data = data
            cy.contains(this.data.ProductName).click()
        })
    }

    getFormField(fieldName) {
        return cy.get(`[name="${fieldName}"]`)
    }

    getLabels(labelName) {
        const labels = {Email: 1, Company: 2, Codes: 3}
        return cy.get(`.pgn__form-group:nth-of-type(${labels[labelName]}) label`)
    }

    navigateToCatalogueScreen() {

        cy.get("[data-test-class=\"href inventory\"]").click({force: true})
        cy.contains(" Catalogue ").click({force: true} )
    }

    getHouseholdButton() {
        return cy.get("#navbarDropdownMenuLink-household")
    }

    getProcurementOption() {
        return cy.get("[routerlink=\"/home/procurement\"]")
    }

    getCatalogueOption() {
        return cy.get(".nav > :nth-child(4) > .nav-link")
    }

    getSuccessMessage() {
        return cy.get(".alert-dialog .message")
    }


    validateDuplicateCatalogue() {
        this.getDialogue().should("to.be.visible")
            .should("have.text","Catalog already exists for the given name")
    }
    validateUpdatedCatalogue(){
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        this.getDialogue().should("have.text", "Catalogue Updated successfully!")
    }

    getEditIcon(catalogueName) {
        return cy.contains(catalogueName).parent("[data-test-class=\"td catalogueName\"]").siblings("[data-test-class=\"td actions\"]").children().children("[data-test-class=\"btn editCatalogue\"]")

    }
    editCatalogue(CatalogueName) {
        this.getEditIcon(CatalogueName).click()
    }
    selectCatalogue(CatalogueName) {
        return cy.contains(CatalogueName).parent("[data-test-class=\"td catalogueName\"]").siblings("[data-test-class=\"td sl-No\"]").children("[data-test-class=\"chkbx catalogue\"]")
    }
    getDeleteButton() {
        return cy.contains("Delete")
    }
    getSelectionCheckBox(CatalogueName) {
        return cy.contains(CatalogueName).parent().parent().children(":nth-child(1)")
    }
    deleteCatalogue(CatalogueName) {
        this.selectCatalogue(CatalogueName).click()
        this.getDeleteButton().click({force: true})
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.wait(3000)
        cy.get(".p-confirm-dialog-accept > .p-button-icon").click()
    }

    validateDeletionMessage() {
        this.getDialogue().should("to.be.visible")
            .should("have.text","Selected record(s) deleted successfully!")
    }

    // filterByIndustry(s) {
    //     /* cy.get("placeholder=\"Select Industry\"")
    //         .parent()
    //         .children(".button")
    //         .click()*/
    //
    // }
}

export default CataloguePage
