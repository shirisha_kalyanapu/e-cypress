class LoginPage {
  email = ":nth-child(1) > .form-control"
  password = ":nth-child(2) > .form-control"
  loginButton = ".btn"

  addEmail(value) {
      cy.get(this.email)
          .clear()
          .type(value)
          .should("have.value", value)

  }

  addPassword(value) {
      cy.get(this.password)
          .clear()
          .type(value)
  }

  submitLogin() {
      cy.get(this.loginButton)
          .click()
  }

  getEmailValidation() {
      return cy.get("[id=input-username]")
  }
}

export default LoginPage
