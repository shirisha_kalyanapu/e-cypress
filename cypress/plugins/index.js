/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars

const fs = require("fs-extra")
const path = require("path")

function getConfigurationByFile (file) {
    const pathToConfigFile = path.resolve("config", `${file}.json`)
    return fs.readJson(pathToConfigFile)
}

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
module.exports = (on, config) => {
    const file = config.env.ENVIRONMENT || "business_portal"

    return getConfigurationByFile(file)
}
module.exports = (on, config) => {
    config.env.BASE_URL = process.env.BASE_URL
    config.env.CYPRESS_USER_EMAIL=process.env.CYPRESS_USER_EMAIL
    config.env.CYPRESS_USER_PASSWORD=process.env.CYPRESS_USER_PASSWORD
    config.baseUrl=process.env.BASE_URL
    return config
}
const mysql = require("mysql")

function queryTestDb(query, config) {

    // creates a new mysql connection using credentials from cypress.json env's

    const connection = mysql.createConnection(config.env.db)

    // start connection to db

    connection.connect()

    // exec query + disconnect to db as a Promise

    return new Promise((resolve, reject) => {

        connection.query(query, (error, results) => {

            if (error) reject(error)

            else {

                connection.end()

                console.log(results)

                return resolve(results)

            }

        })

    })

}
module.exports = (on, config) => {

    // Usage: cy.task('queryDb', query)

    on("task", {

        queryDb: query => {

            return queryTestDb(query, config)

        },

    })

    return config

}