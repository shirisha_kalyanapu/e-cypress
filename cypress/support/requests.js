import {generateExternalID, getRandomID} from "./data"

let token
token = Cypress.env("token")
let authorization
authorization = "Bearer " + token
export const createIndustry = () => {
    return cy.request({
        method: "POST", url: "/api/master-data-service/industry", headers: {
            "Authorization": authorization,
        }, body: generateIndustryPayload()
    })
}
export const generateLoginPayload = () => {
    return {
        "email": "shirisha@trimindtech.com", "password": "test12345"
    }
}

export const generateIndustryPayload = () => {
    return {
        "name": "Industry Test " + getRandomID(), "externalId": generateExternalID(), "description": null,
    }
}

export const generateCommodityPayload = (response) => {
    return {
        "name": "Commodity Test" + getRandomID(), "externalId": generateExternalID(), "description": null, "industry": {
            "id": response.body.id,
            "externalId": response.body.externalId,
            "lockVersion": 0,
            "name": response.body.name,
            "description": response.body.description,
            "deleted": false
        }
    }
}

export const createCommodity = (response) => {
    return cy.request({
        method: "POST", url: "/api/master-data-service/commodity", headers: {
            "Authorization": authorization,
        }, body: generateCommodityPayload(response)
    })
}
export const generateCPVPayload = () => {
    return {
        "code": getRandomID(), "description": getRandomID(), "language": {
            "lockVersion": 0, "id": 1, "isoCode": "en", "name": "English", "deleted": false
        }
    }
}

export const createCPV = () => {
    return cy.request({
        method: "POST", url: "api/master-data-service/cpv-code", body: generateCPVPayload()
    })
}

export const createBusinessUnit = () => {
    return cy.request({
        method: "POST", url: "api/master-data-service/business-unit/", body: generateBusinessUnit()
    })
}

export const generateBusinessUnit = () => {
    return {
        "ExternalId": "Automation EXID" + getRandomID(),
        "generalEmailAddress": "Automation" + getRandomID() + "@gmail.com",
        "generalName": "Automation BU Unit" + getRandomID(),
        "postAddressAddress": "Automation post Address" + getRandomID(),
        "postAddressCity": "Automation Hyd" + getRandomID(),
        "postAddressCountry": "Automation India" + getRandomID(),
        "postAddressEmailAddress": "Automation" + getRandomID() + "@gmail.com",
        "postAddressFax": getRandomID(),
        "postAddressPhone": getRandomID(),
        "postAddressPostalCode": getRandomID(),
        "visitAddressAddress": "automation" + getRandomID(),
        "visitAddressCity": "hyd" + getRandomID(),
        "visitAddressCountry": "india",
        "visitAddressEmailAddress": "automation@gmail.com",
        "visitAddressFax": getRandomID(),
        "visitAddressPhone": "7989910017",
        "visitAddressPostalCode": "292134364"
    }
}
export const createDepartment = (response) => {
    return cy.request({
        method: "POST", url: "api/master-data-service/department", headers: {}, body: generateDepartment(response)
    })
    return cy.request({
        method: "GET", url: "api/master-data-service/department/1", headers: {}, body: generateDepartment(response)
    })
}

export const generateDepartment = (response) => {
    return {
        "name": "Automaation Department- " + getRandomID(),
        "externalId": "Automation EXID- " + getRandomID(),
        "description": "Automation DES" + getRandomID(),
        "businessUnitId": {
            "lockVersion": response.lockVersion,
            "id": response.id,
            "externalId": response.externalId,
            "generalName": response.name,
            "generalEmailAddress": response.generalEmailAddress,
            "visitAddressAddress": response.visitAddressAddress,
            "visitAddressPostalCode": response.visitAddressPostalCode,
            "visitAddressCity": response.visitAddressCity,
            "visitAddressCountry": response.visitAddressCountry,
            "visitAddressPhone": response.visitAddressPhone,
            "visitAddressEmailAddress": response.visitAddressEmailAddress,
            "postAddressAddress": response.postAddressAddress,
            "postAddressPostalCode": response.postAddressPostalCode,
            "postAddressCity": response.postAddressCity,
            "postAddressCountry": response.postAddressCity,
            "postAddressPhone": response.postAddressPhone,
            "postAddressEmailAddress": response.postAddressEmailAddress,
            "postAddressFax": response.postAddressFax,
            "department": response.department,
            "deleted": false
        },
        "deleted": false
    }
}
export const createCostCenter = (dep_res, bus_res) => {
    return cy.request({
        method: "POST",
        url: "api/master-data-service/cost-center",
        headers: {},
        body: generateCostCenter(dep_res, bus_res)
    })
}

export const generateCostCenter = (dep_res, bus_res) => {
    return {
        "externalId": "EX Automation" + getRandomID(),
        "name": "costCenter Automation" + getRandomID(),
        "description": "cost center for automation",
        "department": {
            "lockVersion": dep_res.lockVersion,
            "id": dep_res.id,
            "name": dep_res.name,
            "externalId": dep_res.externalId,
            "description": dep_res.description,
            "businessUnitId": {
                "lockVersion": bus_res.lockVersion,
                "id": bus_res.id,
                "externalId": bus_res.externalId,
                "generalName": bus_res.generalName,
                "generalEmailAddress": bus_res.generalEmailAddress,
                "generalWebsite": bus_res.generalWebsite,
                "generalMemo": bus_res.generalMemo,
                "visitAddressAddress": bus_res.visitAddressAddress,
                "visitAddressPostalCode": bus_res.visitAddressPostalCode,
                "visitAddressCity": bus_res.visitAddressCity,
                "visitAddressCountry": bus_res.visitAddressCountry,
                "visitAddressPhone": bus_res.visitAddressPhone,
                "visitAddressEmailAddress": bus_res.visitAddressEmailAddress,
                "visitAddressFax": bus_res.visitAddressFax,
                "postAddressAddress": bus_res.postAddressAddress,
                "postAddressPostalCode": bus_res.postAddressPostalCode,
                "postAddressCity": bus_res.postAddressCity,
                "postAddressCountry": bus_res.postAddressCountry,
                "postAddressPhone": bus_res.postAddressPhone,
                "postAddressEmailAddress": bus_res.postAddressEmailAddress,
                "postAddressFax": bus_res.postAddressFax,
                "department": bus_res.department,
                "deleted": false
            },
            "deleted": false
        },
        "deleted": false
    }
}

//{"lockVersion":0,"createdById":21,"createdByName":"shirisha kalyanapu","createdDate":"2022-02-24 18:03","lastModifiedById":21,"lastModifiedByName":"shirisha kalyanapu","lastModifiedDate":"2022-02-24 18:03","id":45,"externalId":"8778","name":"6567","description":"67578","department":{"lockVersion":1,"createdById":null,"createdByName":null,"createdDate":"2021-06-10 06:39","lastModifiedById":null,"lastModifiedByName":"shirisha kalyanapu","lastModifiedDate":"2021-09-15 04:49","id":3,"name":"IT Department","externalId":"DE03","description":"IT Department","businessUnitId":{"lockVersion":1,"createdById":null,"createdByName":null,"createdDate":"2021-06-10 06:39","lastModifiedById":null,"lastModifiedByName":null,"lastModifiedDate":"2021-06-10 06:39","id":1,"externalId":"BU01","generalName":"IT Business Unit","generalEmailAddress":"trimindtech@gmail.com","generalWebsite":null,"generalMemo":null,"visitAddressAddress":"Troubadour","visitAddressPostalCode":"1181","visitAddressCity":"Amstelveen","visitAddressCountry":"Netherlands","visitAddressPhone":"9823044990","visitAddressEmailAddress":"it-bu@eprocure.eu","visitAddressFax":null,"postAddressAddress":"Hyderabad","postAddressPostalCode":"5000890","postAddressCity":"Hyderabad","postAddressCountry":"India","postAddressPhone":"9876712129","postAddressEmailAddress":"trimindtech@gmail.com","postAddressFax":null,"department":null,"deleted":false},"deleted":false},"deleted":false}

export const createCostAccount = (costCenter_res, dep_res, bus_rep) => {
    return cy.request({
        method: "POST",
        url: "api/master-data-service/cost-account",
        headers: {},
        body: generateDepartment(costCenter_res, dep_res, bus_rep)
    })
}

export const generateCostAccount = (costCenter_res, dep_res, bus_rep) => {
    return {
        "externalId": getRandomID(),
        "name": "costAccount" + getRandomID(),
        "description": "description",
        "costCenter": {
            "lockVersion": costCenter_res.lockVersion,
            "createdById": costCenter_res.lockVersion,
            "createdByName": costCenter_res.createdByName,
            "createdDate": costCenter_res.createdDate,
            "lastModifiedById": costCenter_res.lastModifiedById,
            "lastModifiedByName": costCenter_res.lastModifiedByName,
            "lastModifiedDate": costCenter_res.lastModifiedDate,
            "id": costCenter_res.id,
            "externalId": costCenter_res.externalId,
            "name": costCenter_res.name,
            "description": costCenter_res.description,
            "department": {
                "lockVersion": dep_res.lockVersion,
                "createdById": dep_res.createdById,
                "createdByName": dep_res.createdByName,
                "createdDate": dep_res.createdDate,
                "lastModifiedById": dep_res.lastModifiedById,
                "lastModifiedByName": dep_res.lastModifiedByName,
                "lastModifiedDate": dep_res.lastModifiedDate,
                "id": dep_res.id,
                "name": dep_res.name,
                "externalId": dep_res.externalId,
                "description": dep_res.description,
                "businessUnitId": {
                    "lockVersion": bus_rep.lockVersion,
                    "createdById": bus_rep.createdById,
                    "createdByName": bus_rep.createdByName,
                    "createdDate": bus_rep.createdDate,
                    "lastModifiedById": bus_rep.lastModifiedById,
                    "lastModifiedByName": bus_rep.lastModifiedByName,
                    "lastModifiedDate": bus_rep.lastModifiedDate,
                    "id": bus_rep.id,
                    "externalId": bus_rep.externalId,
                    "generalName": bus_rep.generalName,
                    "generalEmailAddress": bus_rep.generalEmailAddress,
                    "generalWebsite": bus_rep.generalWebsite,
                    "generalMemo": bus_rep.generalMemo,
                    "visitAddressAddress": bus_rep.visitAddressAddress,
                    "visitAddressPostalCode": bus_rep.visitAddressPostalCode,
                    "visitAddressCity": bus_rep.visitAddressCity,
                    "visitAddressCountry": bus_rep.visitAddressCountry,
                    "visitAddressPhone": bus_rep.visitAddressPhone,
                    "visitAddressEmailAddress": bus_rep.visitAddressEmailAddress,
                    "visitAddressFax": bus_rep.visitAddressAddress,
                    "postAddressAddress": bus_rep.postAddressAddress,
                    "postAddressPostalCode": bus_rep.postAddressPostalCode,
                    "postAddressCity": bus_rep.postAddressCity,
                    "postAddressCountry": bus_rep.postAddressCountry,
                    "postAddressPhone": bus_rep.postAddressPhone,
                    "postAddressEmailAddress": bus_rep.postAddressEmailAddress,
                    "postAddressFax": bus_rep.postAddressFax,
                    "department": bus_rep.department,
                    "deleted": false
                },
                "deleted": false
            },
            "deleted": false
        },
        "generalLedger": null,
        "deleted": false
    }
}
export const createGeneralLedger = (costAccount_res, costCenter_res, dep_res, bus_res) => {
    return cy.request({
        method: "POST",
        url: "api/master-data-service/general-ledger",
        headers: {},
        body: generateDepartment(costAccount_res, costCenter_res, dep_res, bus_res)
    })
}

export const generateGeneralLedger = (costAccount_res, costCenter_res, dep_res, bus_res) => {
    return {
        "id": getRandomID(),
        "externalId": "Ex automation" + getRandomID(),
        "name": "GL" + getRandomID(),
        "description": "12321",
        "costAccount": {
            "lockVersion": costAccount_res.lockVersion,
            "createdById": costAccount_res.createdById,
            "createdByName": costAccount_res.createdByName,
            "createdDate": costAccount_res.createdDate,
            "lastModifiedById": costAccount_res.lastModifiedById,
            "lastModifiedByName": costAccount_res.lastModifiedByName,
            "lastModifiedDate": costAccount_res.lastModifiedDate,
            "id": costAccount_res.id,
            "externalId": costAccount_res.externalId,
            "name": costAccount_res.name,
            "description": costAccount_res.description,
            "costCenter": {
                "lockVersion": costCenter_res.lockVersion,
                "createdById": costCenter_res.createdById,
                "createdByName": costCenter_res.createdByName,
                "createdDate": costCenter_res.createdDate,
                "lastModifiedById": costCenter_res.lastModifiedById,
                "lastModifiedByName": costCenter_res.lastModifiedByName,
                "lastModifiedDate": costCenter_res.lastModifiedDate,
                "id": costCenter_res.id,
                "externalId": costCenter_res.externalId,
                "name": costCenter_res.name,
                "description": costAccount_res.description,
                "department": {
                    "lockVersion": dep_res.lockVersion,
                    "createdById": dep_res.createdById,
                    "createdByName": dep_res.createdByName,
                    "createdDate": dep_res.createdDate,
                    "lastModifiedById": dep_res.lastModifiedById,
                    "lastModifiedByName": dep_res.lastModifiedByName,
                    "lastModifiedDate": dep_res.lastModifiedDate,
                    "id": dep_res.id,
                    "name": dep_res.name,
                    "externalId": dep_res.externalId,
                    "description": dep_res.description,
                    "businessUnitId": {
                        "lockVersion": bus_res.lockVersion,
                        "createdById": bus_res.createdById,
                        "createdByName": bus_res.createdByName,
                        "createdDate": bus_res.created,
                        "lastModifiedById": bus_res.lastModifiedById,
                        "lastModifiedByName": bus_res.lastModifiedByName,
                        "lastModifiedDate": bus_res.lastModifiedDate,
                        "id": bus_res.id,
                        "externalId": bus_res.externalId,
                        "generalName": bus_res.generalName,
                        "generalEmailAddress": bus_res.generalEmailAddress,
                        "generalWebsite": bus_res.generalWebsite,
                        "generalMemo": bus_res.generalMemo,
                        "visitAddressAddress": bus_res.visitAddressAddress,
                        "visitAddressPostalCode": bus_res.visitAddressPostalCode,
                        "visitAddressCity": bus_res.visitAddressCity,
                        "visitAddressCountry": bus_res.visitAddressCountry,
                        "visitAddressPhone": bus_res.visitAddressPhone,
                        "visitAddressEmailAddress": bus_res.visitAddressEmailAddress,
                        "visitAddressFax": bus_res.visitAddressFax,
                        "postAddressAddress": bus_res.postAddressAddress,
                        "postAddressPostalCode": bus_res.postAddressPostalCode,
                        "postAddressCity": bus_res.postAddressCity,
                        "postAddressCountry": bus_res.postAddressCountry,
                        "postAddressPhone": bus_res.postAddressPhone,
                        "postAddressEmailAddress": bus_res.postAddressEmailAddress,
                        "postAddressFax": bus_res.postAddressFax,
                        "department": bus_res.department,
                        "deleted": bus_res.deleted
                    },
                    "deleted": false
                },
                "deleted": true
            },
            "generalLedger": null,
            "deleted": false
        },
        "deleted": false
    }
}

export const PaymentTerm = () => {
    return cy.request({
        method: "POST", url: "api/master-data-service/general-ledger", headers: {}, body: PaymentTerm()
    })
}

export const generatePaymentTerm = () => {
    return {
        //"name": ,
        "description": "string",
        "settlementPeriod": 0,
        "notificationWindow": 0,
        "sendNotification": true,
        "createdBy": "string",
        "creationDate": "2022-02-26T11:50:08.610Z",
        "updatedBy": "string",
        "updationDate": "2022-02-26T11:50:08.610Z",
        "deleted": true
    }
}
export const generateCategoryType = () => {
    return {
        "name": "name "+getRandomID(),
        "description": "automation",
        "createdBy": "string",
        "creationDate": "2022-02-26T11:50:08.610Z",
        "updatedBy": "string",
        "updationDate": "2022-02-26T11:50:08.610Z",
        "deleted": true
    }
}

//{"lockVersion":0,"createdById":21,"createdByName":"shirisha kalyanapu","createdDate":"2022-02-24 18:10","lastModifiedById":21,"lastModifiedByName":"shirisha kalyanapu","lastModifiedDate":"2022-02-24 18:10","id":43,"externalId":"asdasf","name":"rewtwtr","description":"afsd","costAccount":{"lockVersion":0,"createdById":null,"createdByName":"louklie s","createdDate":"2021-11-25 04:30","lastModifiedById":null,"lastModifiedByName":"louklie s","lastModifiedDate":"2021-11-25 04:30","id":101,"externalId":null,"name":"GD","description":"xxxx","costCenter":{"lockVersion":0,"createdById":null,"createdByName":"shirisha kalyanapu","createdDate":"2021-09-15 04:51","lastModifiedById":null,"lastModifiedByName":"shirisha kalyanapu","lastModifiedDate":"2021-09-15 04:51","id":30,"externalId":"EX1209","name":"Supplies","description":"IT Supplies cost center","department":{"lockVersion":1,"createdById":null,"createdByName":null,"createdDate":"2021-06-10 06:39","lastModifiedById":null,"lastModifiedByName":"shirisha kalyanapu","lastModifiedDate":"2021-09-15 04:49","id":3,"name":"IT Department","externalId":"DE03","description":"IT Department","businessUnitId":{"lockVersion":1,"createdById":null,"createdByName":null,"createdDate":"2021-06-10 06:39","lastModifiedById":null,"lastModifiedByName":null,"lastModifiedDate":"2021-06-10 06:39","id":1,"externalId":"BU01","generalName":"IT Business Unit","generalEmailAddress":"trimindtech@gmail.com","generalWebsite":null,"generalMemo":null,"visitAddressAddress":"Troubadour","visitAddressPostalCode":"1181","visitAddressCity":"Amstelveen","visitAddressCountry":"Netherlands","visitAddressPhone":"9823044990","visitAddressEmailAddress":"it-bu@eprocure.eu","visitAddressFax":null,"postAddressAddress":"Hyderabad","postAddressPostalCode":"5000890","postAddressCity":"Hyderabad","postAddressCountry":"India","postAddressPhone":"9876712129","postAddressEmailAddress":"trimindtech@gmail.com","postAddressFax":null,"department":null,"deleted":false},"deleted":false},"deleted":false},"generalLedger":null,"deleted":false},"deleted":false}