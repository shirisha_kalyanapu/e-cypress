// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import "cypress-file-upload"
import "cypress-wait-until"
import LoginPage from "../portal-pages/business-portal/LoginPage"
import login from "../portal-pages/business-portal/LoginPage"
import SupplierLoginPage from "../portal-pages/supplier-portal/Authentication/login-page"
import productsPage from "../portal-pages/supplier-portal/ProcurementModule/ProductsPage"
import {getSelector} from "./data"
//import masterdata from "../fixtures/procurement-module/test_data/master-data"

Cypress.Commands.add("login", () => {
    const login = new LoginPage()

    cy.visit("http://15.207.101.104:8201/")
    cy.get("#input-username").type("shirisha@trimindtech.com")
    cy.get("#input-password").type("test12345")
    cy.contains("Login").click()
    /* login.addEmail(Cypress.env("CYPRESS_EP_USER_EMAIL"))
    login.addPassword(Cypress.env("CYPRESS_EP_USER_PASSWORD"))
    login.submitLogin()*/
})
Cypress.Commands.add("postLoginRequest", () => {
    cy.request({
        headers:
            {"X-DOMAIN": "trimindtech.com"},
        method: "POST",
        url: "/api/client-user-service/user/auth",
        body: {
            "email":Cypress.env("CYPRESS_EP_USER_EMAIL"),
            "password":Cypress.env("CYPRESS_EP_USER_PASSWORD")
        },
    }).as("loginResponse")
        .then((response) => {
            Cypress.env("token", response.body.token)
            // Cypress.Cookies.preserveOnce('token')
            return response
        })
        .its("status")
        .should("eq", 200)
})
Cypress.Commands.add("forceVisit", url => {
    cy.window().then(win => {
        return win.open(url, "_self")
    })
})

let LOCAL_STORAGE_MEMORY = {}

Cypress.Commands.add("saveLocalStorage", () => {
    Object.keys(localStorage).forEach(key => {
        LOCAL_STORAGE_MEMORY[key] = localStorage[key]
    })
})

Cypress.Commands.add("restoreLocalStorage", () => {
    Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
        localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key])
    })
})

Cypress.Commands.add("saveLocalStorage", () => {
    Object.keys(localStorage).forEach(key => {
        LOCAL_STORAGE_MEMORY[key] = localStorage[key]
    })
})
    Cypress.Commands.add("writeToMasterdataFile", (module, response, filePath) => {
        cy.readFile(filePath, (err, data) => {
            if (err) {
                return console.error(err)
            }
        }).then((data) => {
            let moduleName =module
            data,moduleName = response.body
            cy.writeFile(filePath, data)
        })

    })

    Cypress.Commands.add("restoreLocalStorage", () => {
        Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
            localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key])
        })
    })

    Cypress.Commands.add("loginToSupplierPortal", () => {
        const supplierLogin = new SupplierLoginPage()

        /* cy.visit("http://15.207.101.104:8221/")
         supplierLogin.addEmail("shirisha.k@trimindtech.com")
         supplierLogin.addPassword("test12345")
         supplierLogin.submitLogin()*/
        cy.visit(Cypress.env("URL"))
        supplierLogin.addEmail(Cypress.env("CYPRESS_EP_USER_EMAIL"))
        supplierLogin.addPassword(Cypress.env("CYPRESS_EP_USER_PASSWORD"))
        supplierLogin.submitLogin()
    })


    Cypress.Commands.add("navigateToModule", (module) => {


    })
    Cypress.Commands.add("randomName", () => {
        const uuid = () => Cypress._.random(0, 1e6)
        const id = uuid()
        const testname = `testname${id}`
        return testname
    })


    Cypress.Commands.add("loginRequest", () => {
        cy.request({
            method: "POST",
            url: "http://15.207.101.104:8201/api/client-user-service/user/auth",
            body: {
                email: "roja.malladi@trimindtech.com",
                password: "test12345",
            },

        })
            .then((resp) => {
                window.localStorage.setItem("jwt", resp.body.user.token)
            })
    })

    const PDF_FILE = "Test.pdf"
    const DOCX_FILE = "Test.docx"

    const IMG_FILE = "images/image1.jpg"


    Cypress.Commands.add("uploadMultipleFiles", ({count = 3, type = "docx", IMG_FILE}) => {
        const files = []

        const fileTypes = {
            docx: DOCX_FILE,
            pdf: PDF_FILE,
            jpg: IMG_FILE,
        }

        // eslint-disable-next-line no-plusplus
        for (let index = 0; index < count; index++) {
            if (index % 3 === 0) {
                files.push(fileTypes[type] || fileTypes.docx)
            } else {
                files.push(fileTypes[type] || fileTypes.jpg)
            }
        }
        // eslint-disable-next-line no-undef
        productsPage.getProductImages()
            .attachFile(files)
    })

    Cypress.Commands.add("getDialog", () => cy.get(".p-confirm-dialog-message"))
    Cypress.Commands.add("getBySel", (selector, ...args) => {
        return cy.get(`[data-test-class="${selector}"]`, ...args)
    })
    Cypress.Commands.add("getSelector", (selector, ...args) => {
        var attribute = getSelector(selector)
        return "[data-test-class=" + attribute + "]"
    })
    Cypress.Commands.overwrite("request", (originalFn, ...options) => {
        const optionsObject = options[0]
        const token = Cypress.env("token")

        if (!!token && optionsObject === Object(optionsObject)) {
            optionsObject.headers = {
                authorization: "Bearer " + token,
                ...optionsObject.headers,
            }

            return originalFn(optionsObject)
        }

        return originalFn(...options)
    })