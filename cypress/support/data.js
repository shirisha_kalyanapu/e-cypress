import moment from "moment-timezone"

export function getRandomID() {
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()
    const testname = `ID${id}`
    return testname
}

export function getRandomName() {
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()
    const testname = `Test Name ${id}`
    return testname
}
export function generateExternalID() {
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()
    const testname = `EX ${id}`
    return testname
}
export function getTodayDate() {
    var todaysDate = moment().format("DD")
    var formatedDate =todaysDate.replace(/^0+/, "")
    return formatedDate
}
export function getFutureDate() {
    var todaysDate = moment().add(4,"days").format("DD")
    var formatedDate =todaysDate.replace(/^0+/, "")
    return formatedDate
}
export function getNextDay() {
    var todaysDate = moment().add(1,"days").format("DD")
    var formatedDate =todaysDate.replace(/^0+/, "")
    return formatedDate
}
export function getSelector(selector) {
    var attribute = "\""+selector+"\""
    return attribute
}

