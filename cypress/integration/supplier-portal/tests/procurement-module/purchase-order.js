import purchaseOrder from "../../../../portal-pages/supplier-portal/ProcurementModule/purchase-order"
import poDetails from "../../../../fixtures/business-portal/procurement-module/test_data/sp-purchase-order.json"


describe("Application login", function () {
    const purchaseorder = new purchaseOrder()
    beforeEach(() => {
        cy.loginToSupplierPortal()
        cy.title()
            .should("be.equal", "EProcure - Supplier Portal")
        cy.log("** User logged in to account **")
        cy.restoreLocalStorage()
    })

    it("SP - Validate the purchase order module", function () {
        purchaseorder.validateDashboard()
        purchaseorder.navigateToOrders()
        purchaseorder.clickFilterBy()
        purchaseorder.selectStatus(poDetails.status)
    })

    it("SP - Validate waiting for delivery PO", function () {
        purchaseorder.navigateToOrders()
        purchaseorder.clickFilterBy()
        purchaseorder.selectStatus(poDetails.status)
        purchaseorder.checkPurchaseorder(poDetails.purchaseOrder)
        purchaseorder.clickUpdateDeliverybutton()
        purchaseorder.provideDeliveryQuantity(poDetails.quantity)
        purchaseorder.clickSaveButton()
    })

    it("SP-Validate Partially Delivered PO", function () {
        purchaseorder.navigateToOrders()
        purchaseorder.clickFilterBy()
        purchaseorder.selectPartiallyDeliveredStatus(poDetails.partiallyDelivered)
        purchaseorder.checkPurchaseorder(poDetails.partiallyDeliveredPO)
        purchaseorder.clickUpdateDeliverybutton()
        purchaseorder.provideDeliveryQuantity(poDetails.quantity)
        purchaseorder.clickSaveButton()
    })

    it("SP-Validate Delivered PO", function () {
        purchaseorder.navigateToOrders()
        purchaseorder.clickFilterBy()
        purchaseorder.selectDeliveryStatus(poDetails.Delivered)
        purchaseorder.checkPurchaseorder(poDetails.DeliveredPO)
        purchaseorder.validateUpdateDeliveryButton()
    })


    it("SP- validate invalid PO search", function () {
        purchaseorder.validateDashboard()
        purchaseorder.navigateToOrders()
        purchaseorder.clickFilterBy()
        purchaseorder.naviagteToDeliveryscreen()
        purchaseorder.getSearchBox(poDetails.invalidPo)
        purchaseorder.clickSearchBox()
        purchaseorder.validateInvalidateSearch()
    })

    afterEach(() => {
        purchaseorder.clickLogout()
        cy.saveLocalStorage()
    })

})