import ProductsPage from "../../../../portal-pages/supplier-portal/ProcurementModule/ProductsPage"
import productDetails from "../../../../fixtures/business-portal/procurement-module/product-details.json"

describe("Application login", function () {
    const product = new ProductsPage()
    before("loginToApplication", () => {
        cy.loginToSupplierPortal()
        cy.title()
            .should("be.equal", "EProcure - Supplier Portal")
        cy.log("** User logged in to account **")
        product.navigateToProductScreen()
    })

    beforeEach(" SP- Validate the catalogue screen details ", function () {
        cy.restoreLocalStorage()

    })

    it("SP- Validate the product creation", function () {
        product.createProduct(productDetails)
        product.clickReorderPoint()
        product.provideReorderDetails(productDetails)
        product.clickProductAttributes()
        product.provideProductAttributes(productDetails)
        product.clickSubmit()
        product.validateProductCreated(productDetails.ProductName)
    })

    it("SP - update the product details ", function () {
        product.navigateToProductScreen()
        product.openEditProductView(productDetails.ProductName)
        product.updateDescription(productDetails.updatedDescription)
        product.validateUpdatedDescDetails(productDetails.ProductName, productDetails.updatedDescription)
    })
    it("SP - validate Deletion the product", function () {
        product.navigateToProductScreen()
        product.deleteProduct(productDetails.ProductName)
    })

    it("SP - Validate save disabled when there is incomplete data", function () {
        product.openNewProductPage()
        product.addProductCode(productDetails.ProductCode)
        product.validateSaveButtonDisabled()
    })

    it("SP - Validate the Product code required ", function () {
        product.navigateToProductScreen()
        product.openNewProductPage()
        product.addProductCode(productDetails.ProductCode)
        product.validateProductCodeRequiredMessage(productDetails.ProductCodeRequiredMessage)
    })

    it("SP - Validate the Product Name Limit validation ", function () {

        product.addProductName(productDetails.ProductNameMoreThan26)
        product.validateProductNameErrorMessage(productDetails.ProductNameLimitValidationMessage)
    })
    it("SP - Validate the Product Name required ", function () {

        product.clearProductField()
        product.validateProductNameErrorMessage(productDetails.ProductNameRequiredMessage)

    })

    it("SP - Validate the At least one Price Details required ", function () {
        product.gotoPriceTab()
        product.validatePriceErrorMessage(productDetails.PriceRequiredMessage)
    })
    afterEach(() => {
        cy.saveLocalStorage()
    })
})
