import invoicePage from "../../../../portal-pages/supplier-portal/ProcurementModule/invoice-page"
import invoiceDetails from "../../../../fixtures/business-portal/procurement-module/test_data/sp-invoice.json"
import purchaseOrder from "../../../../portal-pages/supplier-portal/ProcurementModule/purchase-order"
import poDetails from "../../../../fixtures/business-portal/procurement-module/test_data/sp-purchase-order.json"



describe("Application login", function () {
    const invoice = new invoicePage()
    const purchaseorder = new purchaseOrder()
    beforeEach(() => {
        cy.loginToSupplierPortal()
        cy.title()
            .should("be.equal", "EProcure - Supplier Portal")
        cy.log("** User logged in to account **")
        cy.restoreLocalStorage()
    })
    it("CP - validate invoice creation", function () {
        invoice.clickInvoice()
        invoice.clickNewInvoice()
        invoice.enterGeneralDetails(invoiceDetails)
        invoice.selectReceivedPO(invoiceDetails.po)
        invoice.providePaymentTerm(invoiceDetails.paymentTerm)
        invoice.enterInvoiceLineItems(invoiceDetails)
        invoice.enterNonProductLineItems(invoiceDetails)
        invoice.clickInvoiceApprovers()
        invoice.addApprover()
        invoice.clickSaveButton()
        invoice.validateCreatedInvoice(invoiceDetails.successMessage)
        invoice.clickOkButton()
    })
    it("CP - validate user able to create duplicate invoice", function () {
        invoice.clickInvoice()
        invoice.clickNewInvoice()
        invoice.enterGeneralDetails(invoiceDetails)
        invoice.selectReceivedPO(invoiceDetails.po)
        invoice.providePaymentTerm(invoiceDetails.paymentTerm)
        invoice.clickInvoiceDetails()
        invoice.validatedAlreadyCreatedInvoice(invoiceDetails.validationMessage)
    })

    afterEach(() => {
        cy.wait(3000)
        invoice.clickLogout()
        cy.saveLocalStorage()
    })


})