// / <reference types="cypress" />
import CataloguePage from "../../../../portal-pages/supplier-portal/ProcurementModule/CataloguePage"
import catalogData from "../../../../fixtures/business-portal/procurement-module/catalogue-details.json"

describe("Cataogue module tests", function () {
    const catalogue = new CataloguePage()
    before("ApplicationLogin", () => {
        cy.loginToSupplierPortal()
        cy.title()
        cy.log("** User logged in to account **")

    })

    beforeEach(" SP- Validate the catalogue screen details ", function () {
        cy.restoreLocalStorage()
        catalogue.navigateToCatalogueScreen()

    })
    it(" SP - Validate the catalogue name limits entry to 50 characters ", function () {
        catalogue.getNewCatalogue().click()
        catalogue.addCatalogueName(catalogData.CatalogueNameWithchar)
        catalogue.validateCatalogueName()
    })

    it("SP - Validate the catalogue description limits entry to 259 characters ", function () {
        catalogue.getNewCatalogue().click()
        catalogue.addCatalogueDescription(catalogData.CatalogueDescrptionWithchar)
        catalogue.validateDescription()
    })

    it("SP - Validate the required products validation", function () {
        catalogue.getNewCatalogue().click()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.attachCoverImage(catalogData.image1)
        catalogue.addProduct(catalogData.extproduct)
        cy.get("[label=\"Submit\"]")
            .click()
    })

    it("SP - Validate the creation of catalogue", function () {
        catalogue.getNewCatalogue().click()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.attachCoverImage(catalogData.image1)
        catalogue.addProduct(catalogData.extproduct)
        catalogue.clickSubmitButton()
        catalogue.clickSaveButton()
        catalogue.validateCatalogueCreated(catalogData.catalogueName)
    })

    it("SP - Validate the duplicate catalogue creation", function () {
        catalogue.getNewCatalogue().click()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.attachCoverImage(catalogData.image1)
        catalogue.addProduct(catalogData.extproduct)
        cy.get("[label=\"Submit\"]")
            .click()
        catalogue.clickSaveButton()
        catalogue.validateDuplicateCatalogue()
    })

    it("SP - update the catalogue details ", function () {
        catalogue.editCatalogue(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.updatedDescription)
        catalogue.clickSaveButton()
        catalogue.validateUpdatedCatalogue()
    })
    it("SP - Validate deletion the catalogue", function () {
        catalogue.deleteCatalogue(catalogData.catalogueName)

    })

    it("SP - Validate the catalogue cover image required", function () {
        catalogue.getNewCatalogue()
            .click()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.addProduct(catalogData.extproduct)
        catalogue.clickSaveButton()
    })

    afterEach(() => {
        cy.saveLocalStorage()
    })

})


