/// <reference types="cypress" />
import LoginPage from "../../../../PageObjects/BusinessPortal/LoginPage"
import CatalogBuying from "../../../../PageObjects/BusinessPortal/PurchaseOrderModule/CatalogBuying"
describe("Application login", function () {
    const loginPage = new LoginPage()
    const catalogBuying= new CatalogBuying()
    before(() => {
        cy.fixture("userCredentials").then(function (data) {
            this.data = data
            cy.login(this.data.UserEmail,this.data.Password)
        })
        cy.title().should("be.equal","EProcure Business Portal")
        cy.log("** User logged in to account **")
    })
    /*it('validate catalog buying  screen', function () {
        cy.contains('[data-cy=email-validations]', 'invalid credentials')
    });*/
    it("validate images displayed on the catalog buying screen", () => {
        catalogBuying.clickOnCatalogueBuyingLink()
        catalogBuying.getImages()
    })

    it("validate displayed catalogue supplier name ", () => {
        catalogBuying.validateCatalogSupplierName()
    })

})
