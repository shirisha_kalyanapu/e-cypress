import ContractsPage from "../../../../portal-pages/business-portal/contract/contracts-page"
import contractDetails from "../../../../fixtures/business-portal/procurement-module/test_data/contracts.json"


describe("CataogueBuying module tests", function () {

    const contract = new ContractsPage()
    before("ApplicationLogin", () => {
        cy.login()
        cy.title()
            .should("be.equal", "EProcure Business Portal")
        cy.log("** User logged in to account **")
    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })
    it("CP - Check contract creation", function () {
        contract.navigateToContracts()
        contract.clickNewContract()
        contract.enterContractDetails(contractDetails)
        contract.provideDepartment(contractDetails)
        contract.provideCostCenter(contractDetails)
        contract.provideClientcontact(contractDetails)
        contract.provideSuppliercontact(contractDetails)
        contract.addApprovers()
        contract.selectApprover(contractDetails.approver)
        contract.selectContractTemplate(contractDetails.template)
        contract.validateSubmitForReviewButton()
        contract.clickSendForReviewButton()
        contract.validateCreatedContracts(contractDetails.successMessage)
        contract.clickOkButton()

    })
    it(" CP - Check Approval flow of contract", function () {
        contract.goToConfiguration()
        contract.checkContract()
        contract.approveContract()
        contract.navigateToContracts()
        contract.validateApprovedContractStatus()
    })

    it("CP - Check draft functionality of contract", function () {
        contract.navigateToContracts()
        contract.clickNewContract()
        contract.enterDraftContractDetails(contractDetails)
        contract.provideDepartment(contractDetails)
        contract.provideCostCenter(contractDetails)
        contract.provideClientcontact(contractDetails)
        contract.clickDraftButton()
        contract.validateDraftedContract()


    })

    it("CP - Check updating the draft functionality", function () {
        contract.navigateToContracts()
        contract.checkContractId()
        contract.provideClientcontact(contractDetails)
        contract.provideSuppliercontact(contractDetails)
        contract.addApprovers()
        contract.selectApprover(contractDetails.approver)
        contract.selectContractTemplate(contractDetails.template)
        contract.validateSubmitForReviewButton()
        contract.clickSendForReviewButton()
        contract.validateSubmittedContract()
    })

    it("CP - Check Rejection contract", function () {
        contract.goToConfiguration()
        contract.checkContract()
        contract.rejectContract()
        contract.navigateToContracts()
        contract.validateApprovedContractStatus()
        contract.contractDbValidation()

    })

    afterEach(() => {
        cy.saveLocalStorage()
    })


})
