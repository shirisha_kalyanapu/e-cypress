// / <reference types="cypress" />
//import masterdata from "../fixtures/procurement-module/test_data/master-data"
import {createBusinessUnit, createCPV, createDepartment, generateLoginPayload} from "../../../../support/requests"

let businessUnit
let department
let costCenter
let costAccount


describe("Data generation for the master data", function () {
    before("ApplicationLogin", () => {
        cy.postLoginRequest()
    })

    it("CP - Create CPV Code ", function () {
        createCPV().then((response) => {
            expect(response).property("status").to.equal(201)
            cy.writeToMasterdataFile("cpvCode",response, "cypress/fixtures/business-portal/procurement-module/test_data/master-data.json" )
        })
    })

    it("CP - Create Business Unit ", function () {
        createBusinessUnit().then((response) => {
            expect(response).property("status").to.equal(201)
            cy.log(JSON.stringify(response.body))
            cy.writeToMasterdataFile("businessUnit",response, "cypress/fixtures/business-portal/procurement-module/test_data/master-data.json" )
            businessUnit = response.body

        })
    })
    it("CP - Create Department Code ", function () {
        cy.log(businessUnit.id)
        createDepartment(businessUnit).then((response) => {
            expect(response).property("status").to.equal(201)
            cy.log(JSON.stringify(response.body))
            department = response.body
        })

    })
    /* it("CP-Create CostCenter", function () {

         createCostCenter(department,businessUnit,costCenter).then((response) => {
             expect(response).property("status").to.equal(201)
             cy.log(JSON.stringify(response.body))
             costCenter = response.body
         })
     })

     it("CP - Create CostAccount", function () {

         createCostAccount(costCenter,department,businessUnit,).then((response) => {
             expect(response).property("status").to.equal(201)
             cy.log(JSON.stringify(response.body))
             costAccount = response.body
         })
     })

     it("CP - Create GeneralLedger", function () {

         createCostAccount(costAccount,businessUnit,costCenter,businessUnit).then((response) => {
             expect(response).property("status").to.equal(201)
             cy.log(JSON.stringify(response.body))
             costAccount= response.body
         })
     })
 */

    afterEach(() => {
        cy.saveLocalStorage()
    })


})