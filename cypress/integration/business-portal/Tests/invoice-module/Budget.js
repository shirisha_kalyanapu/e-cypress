import BudgetPage from "../../../../portal-pages/business-portal/procurement-module/Budget-page"
import budgetDetails from "../../../../fixtures/business-portal/procurement-module/test_data/budjet.json"

describe("Budget module tests", function () {

    const budget = new BudgetPage()
    before("ApplicationLogin", () => {
        cy.login()
        cy.title()
            .should("be.equal", "EProcure Business Portal")
        cy.log("** User logged in to account **")
    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    it("CP - Check Budget Creation with empty fields", function () {
        budget.navigateToBudget()
        budget.clickNewBudget()
        cy.wait(3000)
        budget.clickSaveAsDraft()

        budget.validateTheBudgetCreation()
    })

    it("CP - Validate SendForReview button", function () {
        budget.navigateToBudget()
        budget.clickNewBudget()
        budget.validateTheSubmitButton()
    })

    it(" CP - Check Budget Creation", function () {
        budget.navigateToBudget()
        budget.clickNewBudget()
        budget.provideBudgetDetials(budgetDetails)
        budget.clickBudgetApprovers()
        budget.selectBudgetApprover(budgetDetails.approver)
        budget.navigateToBudgetOwners()
        budget.selectBudgetOwner(budgetDetails.owner)
        budget.clickSubmitButton()
    })

    it(" CP - Validate Dublicate budget Creation", function () {
        budget.navigateToBudget()
        budget.clickNewBudget()
        budget.provideBudgetDetials(budgetDetails)
        budget.clickBudgetApprovers()
        budget.selectBudgetApprover(budgetDetails.approver)
        budget.navigateToBudgetOwners()
        budget.selectBudgetOwner(budgetDetails.owner)
        budget.clickSubmitButton()
        budget.validateDuplicateBudget()
    })

    it("CP - Check Approve Budget", function () {
        budget.goToConfiguration()
        budget.checkContract(budgetDetails.title)
        budget.approveBudget()
    })

    it("CP - Check save as draft with budget", function () {
        budget.navigateToBudget()
        budget.clickNewBudget()
        budget.provideDraftBudgetDetials(budgetDetails)
        budget.clickSaveAsDraft()
    })

    it("CP - Check save as draft with budget", function () {
        budget.navigateToBudget()
        budget.updateBudget(budgetDetails.draftTitle)
        budget.clickBudgetApprovers()
        budget.selectBudgetApprover(budgetDetails.approver)
        budget.navigateToBudgetOwners()
        budget.selectBudgetOwner(budgetDetails.owner)
        budget.clickSubmitButton()
    })

    it("CP - Check Reject Budget", function () {
        budget.goToConfiguration()
        budget.checkContract(budgetDetails.draftTitle)
        budget.rejectBudget()
    })


    afterEach(() => {
        cy.saveLocalStorage()
    })
})