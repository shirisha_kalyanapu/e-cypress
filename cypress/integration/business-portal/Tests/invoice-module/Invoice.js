// / <reference types="cypress" />
import InvoicePage from "../../../../portal-pages/business-portal/procurement-module/invoice-page"
import invoiceDetails from "../../../../fixtures/business-portal/procurement-module/test_data/invoice.json"
import poReceive from "../../../../portal-pages/business-portal/purchase-order-module/POReceive"


describe("CataogueBuying module tests", function () {

    const invoice = new InvoicePage()
    const receive = new poReceive()
    before("ApplicationLogin", () => {
        cy.login()
        cy.title().should("be.equal", "EProcure Business Portal")
        cy.log("** User logged in to account **")

    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    it("CP - Check invoice creation with Approved PO", function () {
        invoice.navigateToInvoiceScreen()
        invoice.clickNewInvoice()
        invoice.enterGeneralSectionDetails(invoiceDetails)
        invoice.selectApprovedPoNumber(invoiceDetails.approvedPo)
        invoice.enterInvoiceLineItemsApprovedPO(invoiceDetails)
        invoice.validateApprovedPO()

    })

    it("CP - Check Creating Invoice with already existing invoice", function () {
        invoice.navigateToInvoiceScreen()
        invoice.clickNewInvoice()
        invoice.enterGeneralDetails(invoiceDetails)
        invoice.selectReceivedPO(invoiceDetails.InvoicePO)
        invoice.validatePOToCreateInvoice(invoiceDetails)
        invoice.clickOkButton()
    })

    it("CP - Check invoice creation with Received PO", function () {
        invoice.navigateToInvoiceScreen()
        invoice.clickNewInvoice()
        invoice.enterGeneralDetails(invoiceDetails)
        invoice.selectReceivedPO(invoiceDetails.po)
        invoice.providePaymentTerm(invoiceDetails.paymentTerm)
        invoice.enterInvoiceLineItems(invoiceDetails)
        invoice.enterNonProductLineItems(invoiceDetails)
        invoice.clickInvoiceApprovers()
        invoice.addApprover(invoiceDetails.approverName)
        //invoice.uploadSupportingDocument(invoiceDetails.filePath)
        invoice.clickSaveButton()
        invoice.validateCreatedInvoice(invoiceDetails.successMessage)
        invoice.clickOkButton()
    })
    it("Check Approval of invoice", function () {
        invoice.goToConfiguration()
        //invoice.checkInvoice()
        invoice.checkInvoiceId()
        invoice.approveInvoice()
        invoice.navigateToInvoiceScreen()
        invoice.validateApprovedInvoiceStatus()
    })

    it("Check reject of invoice", function () {
        invoice.navigateToInvoiceScreen()
        invoice.clickNewInvoice()
        invoice.enterGeneralDetails(invoiceDetails)
        invoice.selectReceivedPO(invoiceDetails.po)
        invoice.providePaymentTerm(invoiceDetails.paymentTerm)
        invoice.enterInvoiceLineItems(invoiceDetails)
        invoice.enterNonProductLineItems(invoiceDetails)
        invoice.clickInvoiceApprovers()
        invoice.addApprover(invoiceDetails.approverName)
        //invoice.uploadSupportingDocument(invoiceDetails.filePath)
        invoice.clickSaveButton()
        invoice.validateCreatedInvoice(invoiceDetails.successMessage)
        invoice.clickOkButton()
        invoice.goToConfiguration()
        invoice.checkInvoice()
        invoice.checkInvoiceId()
        invoice.rejectInvoice()
        invoice.navigateToInvoiceScreen()
        invoice.validateRejectedInvoiceStatus()
    })


    afterEach(() => {
        cy.saveLocalStorage()
    })

})