// / <reference types="cypress" />
import PORPage from "../../../../portal-pages/business-portal/procurement-module/purchase-order-page"
import catalogueBuying from "../../../../portal-pages/business-portal/procurement-module/catalogue-buying-page"
import catalogueBuyingDetails
    from "../../../../fixtures/business-portal/procurement-module/test_data/catalogue-buying.json"
import porDetails
    from "../../../../fixtures/business-portal/procurement-module/test_data/purchase-order-requisition.json"

describe("PO reqquisition  module tests", function () {
    const por = new PORPage()
    const catalogue = new catalogueBuying()
    before("ApplicationLogin", () => {
        cy.login()
    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    it("Check POR listing screen details", function () {
        por.navigateToMarketPalceScreen()
        por.navigateToPORListing()
        por.validateTableColumns()
    })


    it("Create purchase order requistion", function () {
        por.navigateToMarketPalceScreen()
        catalogue.getCatalogueLink(catalogueBuyingDetails.catalogue)
        catalogue.clickAddToCartButton(catalogueBuyingDetails.product)
        catalogue.addMultipleItems(catalogueBuyingDetails)
        catalogue.clickIncreaseQuantity()
        catalogue.clickMyCart()
        catalogue.getSupplierDropdown(catalogueBuyingDetails.supplier)
        catalogue.enterDeliveryAddress(catalogueBuyingDetails)
        catalogue.clickContractSection()
        catalogue.selectContract(catalogueBuyingDetails.contract)
        catalogue.clickCheckoutbutton()
        catalogue.clickYesButton()
        catalogue.validatePOR(catalogueBuyingDetails.successMessage)
        catalogue.clickOKButton()
        catalogue.validateCreatedPOR()
    })

    it("Check user able to create the Purchase order", function () {
        por.selectPOR()
        por.ApprovePOR(porDetails.comments)
        por.validatePO(porDetails.POSuccessMessage)
        por.clickOKButton()
    })

    it("Create purchase order requistion", function () {
        por.navigateToMarketPalceScreen()
        catalogue.getCatalogueLink(catalogueBuyingDetails.catalogue)
        catalogue.clickAddToCartButton(catalogueBuyingDetails.product)
        catalogue.addMultipleItems(catalogueBuyingDetails)
        catalogue.clickIncreaseQuantity()
        catalogue.clickMyCart()
        catalogue.getSupplierDropdown(catalogueBuyingDetails.supplier)
        catalogue.enterDeliveryAddress(catalogueBuyingDetails)
        catalogue.clickContractSection()
        catalogue.selectContract(catalogueBuyingDetails.contract)
        catalogue.clickCheckoutbutton()
        catalogue.clickYesButton()
        catalogue.validatePOR(catalogueBuyingDetails.successMessage)
        catalogue.clickOKButton()
        catalogue.validateCreatedPOR()
    })

    it("Reject Purchase order requistion", function () {
        por.navigateToMarketPalceScreen()
        por.navigateToPORListing()
        por.selectPOR()
        por.rejectPOR(porDetails.rejectComments)
        por.validatePOReject(porDetails.PORejectMessage)
        por.clickOKButton()
        por.validatePORAfterReject()
        por.navigateToPORListing()
        por.validatePORTheStatus(porDetails.poRequisition)
    })

    afterEach(() => {
        cy.saveLocalStorage()
    })
})