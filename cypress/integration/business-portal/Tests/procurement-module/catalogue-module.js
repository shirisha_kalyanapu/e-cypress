/// <reference types="cypress" />
import CataloguePage from "../../../../portal-pages/business-portal/procurement-module/catalogue-page"
import catalogData from "../../../../fixtures/business-portal/procurement-module/catalogue-details.json"

describe("Cataogue module tests", function () {
    const catalogue = new CataloguePage()
    before("ApplicationLogin", () => {
        cy.login()
    })

    beforeEach("CP - Validate the catalogue screen details", function () {
        cy.restoreLocalStorage()
    })
    it("CP - Validate the catalogue name limits entry to 50 characters ", function () {
        catalogue.navigateToCatalogueScreen()
        catalogue.clickCatalogue()
        catalogue.getNewCatalogue()
        catalogue.addCatalogueName(catalogData.CatalogueNameWithchar)
        catalogue.validateCatalogueName()
    })

    it("CP - Validate the catalogue description limits entry to 259 characters ", function () {
        catalogue.navigateToCatalogueScreen()
        catalogue.clickCatalogue()
        catalogue.getNewCatalogue()
        catalogue.addCatalogueDescription(catalogData.CatalogueDescrptionWithchar)
        catalogue.validateDescription()
    })
    it("CP - Validate the required products validation", function () {
        catalogue.navigateToCatalogueScreen()
        catalogue.clickCatalogue()
        catalogue.getNewCatalogue()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.attachCoverImage(catalogData.image1)
        catalogue.validateSaveButton()

    })


    it("CP - Validate the required products validation", function () {
        catalogue.navigateToCatalogueScreen()
        catalogue.clickCatalogue()
        catalogue.getNewCatalogue()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.attachCoverImage(catalogData.image1)
        catalogue.addProduct(catalogData.ProductName)
        catalogue.clickSubmitButton()
        catalogue.clickSaveButton()
        catalogue.validateCreatedCatalogue()
        catalogue.validateCatalogueCreated(catalogData.catalogueName)
    })

    it("CP - Validate the creation of catalogue", function () {
        catalogue.clickCatalogue()
        catalogue.getNewCatalogue()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.attachCoverImage(catalogData.image1)
        catalogue.addProduct(catalogData.ProductName)
        catalogue.clickSubmitButton()
        catalogue.clickSaveButton()
    })

    it("CP - Validate the duplicate catalogue creation", function () {
        catalogue.clickCatalogue()
        catalogue.getNewCatalogue()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.CatalogueType(catalogData.catalogueType)
        catalogue.attachCoverImage(catalogData.image1)
        catalogue.addProduct(catalogData.ProductName)
        catalogue.clickSubmitButton()
        catalogue.clickSaveButton()
        catalogue.validateDuplicateCatalogue()
    })

    it("CP - Update the catalogue details ", function () {
        catalogue.navigateToCatalogueScreen()
        catalogue.clickCatalogue()
        catalogue.editCatalogue(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.updatedDescription)
        catalogue.getSaveButton().click()
        catalogue.validateUpdatedDescDetails(catalogData.catalogueName, catalogData.updatedDescription)
    })

    it("CP - Validate deletion the catalogue", function () {
        catalogue.deleteCatalogue(catalogData.catalogueName)
        catalogue.validateDeletionMessage()
    })

    it("CP - Validate the catalogue cover image required", function () {
        catalogue.clickCatalogue()
        catalogue.getNewCatalogue()
        catalogue.addCatalogueName(catalogData.catalogueName)
        catalogue.addCatalogueDescription(catalogData.description)
        catalogue.addCatalogueType(catalogData.catalogueType)
        catalogue.addProduct(catalogData.ProductName)
        catalogue.clickSubmitButton()
        catalogue.validateSaveButton()
    })

    afterEach(() => {
        cy.saveLocalStorage()
    })


})