// / <reference types="cypress" />
import catalogueBuyingDetails
    from "../../../../fixtures/business-portal/procurement-module/test_data/catalogue-buying.json"
import CatalogueBuyingpage from "../../../../portal-pages/business-portal/procurement-module/catalogue-buying-page"


describe("CataogueBuying module tests", function () {
    const catalogueBuying = new CatalogueBuyingpage()
    before("ApplicationLogin", () => {
        cy.login()
        cy.title().should("be.equal", "EProcure Business Portal")
        cy.log("** User logged in to account **")

    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    it("CP - Check creating POR with zero items ", function () {
        catalogueBuying.navigateToMarketPalceScreen()
        catalogueBuying.getCatalogueLink(catalogueBuyingDetails.catalogue)
        catalogueBuying.clickAddToCartButton(catalogueBuyingDetails.product)
        catalogueBuying.enterQuantity(catalogueBuyingDetails.quantity)
        catalogueBuying.ValidateAddTOCartButton()
        catalogueBuying.clickCancelButton()

    })

    it("CP - Check create PO requisition functionality", function () {
        catalogueBuying.goToMarketPlace()
        catalogueBuying.getCatalogueLink(catalogueBuyingDetails.catalogue)
        catalogueBuying.clickAddToCartButton(catalogueBuyingDetails.product)
        catalogueBuying.addMultipleItems(catalogueBuyingDetails)
        catalogueBuying.clickIncreaseQuantity()
        catalogueBuying.clickMyCart()
        catalogueBuying.getSupplierDropdown(catalogueBuyingDetails.supplier)
        catalogueBuying.enterDeliveryAddress(catalogueBuyingDetails)
        catalogueBuying.clickContractSection()
        catalogueBuying.selectContract(catalogueBuyingDetails.contract)
        catalogueBuying.clickCheckoutbutton()
        catalogueBuying.clickYesButton()
        catalogueBuying.validatePOR(catalogueBuyingDetails.successMessage)
        catalogueBuying.clickOKButton()
        catalogueBuying.validateCreatedPOR()
    })

    it("CP - Check Checkout button without providing required fields", function () {
        catalogueBuying.goToMarketPlace()
        catalogueBuying.getCatalogueLink(catalogueBuyingDetails.catalogue)
        catalogueBuying.clickAddToCartButton(catalogueBuyingDetails.product)
        catalogueBuying.addMultipleItems(catalogueBuyingDetails)
        catalogueBuying.clickIncreaseQuantity()
        catalogueBuying.clickMyCart()
        catalogueBuying.getSupplierDropdown()
        catalogueBuying.enterDeliveryAddress(catalogueBuyingDetails)
        catalogueBuying.clickContractSection()
        catalogueBuying.validateCheckoutbutton()
    })


    afterEach(() => {
        cy.saveLocalStorage()
    })


})