// / <reference types="cypress" />
import ProductsPage from "../../../../portal-pages/business-portal/procurement-module/product-page"
import productDetails from "../../../../fixtures/business-portal/procurement-module/product-details.json"
describe("Application login", function () {
    const product = new ProductsPage()
    before("loginToApplication", () => {
        cy.login()
        product.navigateToProductScreen()
      
    })
    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    it("CP - Validate the product creation", function () {
        product.createProduct(productDetails)
        product.clickSubmitButton()
        product.validateProductCreated(productDetails.ProductName)
    })

    it("CP - update the product details ", function () {
        product.navigateToProductScreen()
        product.openEditProductView(productDetails.ProductName)
        product.updateDescription( productDetails.updatedDescription)
        product.validateUpdatedDescDetails( productDetails.ProductName,productDetails.updatedDescription)
    })

    it("CP - validate Deletion the product", function () {
        product.navigateToProductScreen()
        product.deleteProduct(productDetails.ProductName)
        product.validateDeletedproduct(productDetails.ProductName)
    })

    it("CP - Validate save disabled when there is incomplete data", function () {
        product.openNewProductPage()
        product.addProductCode(productDetails.ProductCode)
        product.validateSaveButtonDisabled()
    })

    it("CP - Validate the Product code required ", function () {
        product.navigateToProductScreen()
        product.openNewProductPage()
        product.addProductCode(productDetails.ProductCode)
        product.validateProductCodeRequiredMessage(productDetails.ProductCodeRequiredMessage)

    })

    it("CP - Validate the Product Name Limit validation ", function () {
        product.addProductName(productDetails.ProductNameMoreThan26)
        product.validateProductNameErrorMessage(productDetails.ProductNameLimitValidationMessage)
    })

    it("CP - Validate the Product Name required ", function () {
        product.clearProductField()
        product.validateProductNameErrorMessage(productDetails.ProductNameRequiredMessage)

    })

    it("CP - Validate the At least one Price Details required ", function () {
        product.gotoPriceTab()
        product.deletePrice()
        product.validatePriceErrorMessage(productDetails.PriceRequiredMessage)
    })

    afterEach(() => {
        cy.saveLocalStorage()
    })
})
