import PurchaseOrderReceive from "../../../../portal-pages/business-portal/purchase-order-module/POReceive"
import POReceiveDetails from "../../../../fixtures/business-portal/procurement-module/test_data/purchase-order-receive.json"

describe("Purchase Order receive module tests", function () {
    const POReceive = new PurchaseOrderReceive()
    before("ApplicationLogin", () => {
        cy.login()
        cy.title().should("be.equal", "EProcure Business Portal")
        cy.log("** User logged in to account **")
    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    it("Validate the catalogueBuying screen details ", function () {
        POReceive.navigateToMarketPalceScreen()
    })

    it("PO Receive", function () {
        POReceive.navigateTOPurchaseorder()
        POReceive.validatePOStatus()
        POReceive.selectPurchaseOrder()
        POReceive.clickReceiveButton()
        POReceive.clickEditIcon()
        POReceive.enterQuantityReceived(POReceiveDetails.receivedQuantity)
        POReceive.enterActualPrice(POReceiveDetails.actualPrice)
        POReceive.clickSaveButton()
    })

    it("Create PO Receive When quantity is morethan ordered", function () {
        POReceive.navigateTOPurchaseorder()
        POReceive.validatePartiallyReceivedStatus()
        POReceive.selectPurchaseOrder()
        POReceive.clickReceiveButton()
        POReceive.clickEditIcon()
        POReceive.enterQuantityReceived(POReceiveDetails.quantity)
        POReceive.enterActualPrice(POReceiveDetails.actualPrice)
        POReceive.enterComments(POReceiveDetails.comments)
        POReceive.clickSaveButton()
    })


    it("SearchValid PO number ", function () {
        POReceive.navigateToPoReceive()
        POReceive.enterValidPONumber(POReceiveDetails.validPo)
        POReceive.clickSearchButton()
        POReceive.validateThePoDetails(POReceiveDetails.validPo)
    })

    it("Validate empty PO search ", function () {
        POReceive.navigateToPoReceive()
        POReceive.getTextBox()
        POReceive.clickSearchButton()
        POReceive.ValidateEmptySearch()

    })

    it("Invalid Search PO number ", function () {
        POReceive.getClearButton()
        POReceive.navigateToPoReceive()
        POReceive.enterValidPONumber(POReceiveDetails.invalidPo)
        POReceive.clickSearchButton()
        POReceive.ValidateInvalidSearch()
    })
    afterEach(() => {
        cy.saveLocalStorage()
    })
})