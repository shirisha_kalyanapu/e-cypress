/// <reference types="cypress" />

import SupplierPage from "../../../../portal-pages/business-portal/supplier-module/supplier-page"
import supplierDetails from "../../../../fixtures/business-portal/supplier-module/supplier-details.json"


describe("Supplier module tests", function () {
    const supplierPage = new SupplierPage()
    before("loginToApplication", () => {
        cy.login()
        Cypress.Cookies.preserveOnce("session_id", "remember_token")
        cy.title()
            .should("be.equal", "EProcure Business Portal")
        cy.log("** User logged in to account **")

    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    beforeEach("Validate the catalogue screen details ", function () {
        supplierPage.navigateToSupplierScreen()

    })
    it("CP- Check Supplier listing screen ", function () {
        supplierPage.validatePagination()
        supplierPage.validateTableColumns()
    })

    it("CP - Check user able to create supplier", function () {
        supplierPage.openNewSupplierPage()
        supplierPage.addGeneralDetails(supplierDetails)
        supplierPage.addContactPerson()
        supplierPage.clickAddButton()
        supplierPage.addAddressDetails(supplierDetails)
        supplierPage.addMailingAddressDetails(supplierDetails)
        supplierPage.getSaveButton().click()
        supplierPage.validateTheSupplierCreation()
    })

    it("CP - Check creating Duplicate Supplier",  function() {
        supplierPage.openNewSupplierPage()
        supplierPage.addGeneralDetails(supplierDetails)
        supplierPage.addAddressDetails(supplierDetails)
        supplierPage.addMailingAddressDetails(supplierDetails)
        supplierPage.addContactPerson()
        supplierPage.clickAddButton()
        supplierPage.getSaveButton().click()
        supplierPage.validateDuplicateSupplierDetails()
    })

    it("CP - Check updating the supplier details", function () {
        supplierPage.navigateToSupplierScreen()
        supplierPage.openEditSupplierView(supplierDetails.name)
        supplierPage.gotoContactperson()
        supplierPage.updateAddressDetails(supplierDetails)
        supplierPage.getSaveButton().click()
        supplierPage.validateUpdatedSupplierDetails()
    })

    it("CP - Check the validations for required fields", function () {
        supplierPage.navigateToSuppliersListing()
        supplierPage.openNewSupplierPage()
        supplierPage.enterSupplierName(supplierDetails.fisrtName)
        supplierPage.validateNameField(supplierDetails.fisrtName, supplierDetails.SupplierNameRequiredMessage)
        supplierPage.getSupplierName()
        supplierPage.validateEmptyNameField(supplierDetails.supplierName, supplierDetails.SupplierNameRequiredMessage)

    })

    it("CP - Validate the email Id is  required ", function () {
        supplierPage.navigateToSuppliersListing()
        supplierPage.openNewSupplierPage()
        supplierPage.enterEmail(supplierDetails)
        supplierPage.validateEmailRequiredErrorMessage(supplierDetails.supplierEmailValidationMessage)
    })

    it("CP - Validate the Site field is required ", function () {
        supplierPage.navigateToSuppliersListing()
        supplierPage.openNewSupplierPage()
        supplierPage.enterSite(supplierDetails)
        supplierPage.validateSupplierSiteRequiredErrorMessage(supplierDetails.supplierSiteValidationMessage)

    })

    it("CP - Validate the save button on providing incomplete data", function () {
        supplierPage.navigateToSuppliersListing()
        supplierPage.openNewSupplierPage()
        supplierPage.enterExternalId(supplierDetails)
        supplierPage.enterBsiCode(supplierDetails)
        supplierPage.enterVestigingNumber(supplierDetails)
        supplierPage.validateSaveButtonDisabled()

    })

    it("Check validation on updating the supplier name ", function () {
        supplierPage.navigateToSupplierScreen()
        supplierPage.openEditSupplierView(supplierDetails.name)
        supplierPage.updateSupplierName(supplierDetails)
        supplierPage.updateSupplierEmail(supplierDetails)
        supplierPage.getSaveButton().click()
        supplierPage.validateUpdatedSupplierName()

    })
    it("validate Deletion the Supplier", function () {
        supplierPage.navigateToSuppliersListing()
        supplierPage.deleteSupplier(supplierDetails.name)

    })

    it("CP - Validate the Supplier Name Limit validation ", function () {
        supplierPage.navigateToSuppliersListing()
        supplierPage.openNewSupplierPage()
        supplierPage.getBusinessAddress().click()
        supplierPage.validateBusinessAddress(supplierDetails.address)
        supplierPage.getBusinessAddress1()
        supplierPage.validateBusinessAddress(supplierDetails.address)
    })



    afterEach(() => {
        cy.saveLocalStorage()
    })
})
