import Question from "../../../fixtures/business-portal/procurement-module/test_data/questionCategory.json"
import QuestionCategory from "../../../portal-pages/business-portal/QuestionCategory"

describe("Supplier module tests", function () {
    const questionCategory = new QuestionCategory()
    before("loginToApplication", () => {
        cy.login()
        Cypress.Cookies.preserveOnce("session_id", "remember_token")
        cy.title()
            .should("be.equal", "EProcure Business Portal")
        cy.log("** User logged in to account **")

    })

    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    beforeEach("Validate the question category screen details ", function () {
        questionCategory.navigateToQuestionCategory()
    })

    it("CP- Validate The vailable table columns ", function () {
        questionCategory.validatePagination()
        questionCategory.validateTableColumns()
    })

    it("CP- Create Question category", function () {
        questionCategory.clickNewQuestionCategory()
        questionCategory.addQuestionCategory(Question)
        questionCategory.clickSaveButton()
        //questionCategory.
    })

    // it("CP- Create Question category", function () {
    //     questionCategory.clickNewQuestionCategory()
    //     questionCategory.addQuestionCategory()
    //     questionCategory.clickSaveButton()
    //     //questionCategory.
    // })





    afterEach(() => {
        cy.saveLocalStorage()
    })
})